/**
	question: https://onlinejudge.org/external/111/11100.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int N = int(1e4);
int B[N];

int main() {
	FastIO();
	int n, i, j, count, cur;
	bool nl = false, ns;
	while (cin >> n, n) {
		for (i = 0; i < n; ++i) cin >> B[i];
		sort(B, B + n);

		// most frequent count
		count = cur = 1;
		for (i = 1; i < n; ++i) {
			if (B[i] == B[i - 1]) {
				++cur;
			} else {
				cur = 1;
			}
			count = max(cur, count);
		}

		if (nl) cout << '\n'; else nl = true;
		cout << count << '\n';
		for (i = 0; i < count; ++i) {
			ns = false;
			for (j = i; j < n; j += count) {
				if (ns) cout << ' '; else ns = true;
				cout << B[j];
			}
			cout << '\n';
		}
	}
	return 0;
}
