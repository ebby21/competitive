/**
	question: https://onlinejudge.org/external/112/11292.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int Size = 20000;
int D[Size], K[Size];

int main() {
	FastIO();
	int n, m, i, j, ans;
	bool doom;
	while (cin >> n >> m, n || m) {
		for (i = 0; i < n; ++i) cin >> D[i];
		for (i = 0; i < m; ++i) cin >> K[i];
		sort(D, D + n);
		sort(K, K + m);
		ans = 0;
		doom = false;
		i = j = 0;
		while (i < n && j < m) {
			while (j < m && K[j] < D[i]) ++j;
			if (j == m) {
				doom = true;
				break;
			}
			ans += K[j];
			++i, ++j;
		}
		if (i < n && j == m) doom = true;
		if  (doom) cout << "Loowater is doomed!\n";
		else       cout << ans << '\n';
	}
	return 0;
}
