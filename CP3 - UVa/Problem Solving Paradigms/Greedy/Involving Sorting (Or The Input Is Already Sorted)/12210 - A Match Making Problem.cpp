/**
	question: https://onlinejudge.org/external/122/12210.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int b, s, i, ans, _i=1, x, b_min;
	while (cin >> b >> s, b || s) {
		cout << "Case " << _i++ << ": ";
		b_min = int(1e9);
		for (i = 0; i < b; ++i) { cin >> x; b_min = min(b_min, x); }
		for (i = 0; i < s; ++i) cin >> x;
		ans = max(b - s, 0);
		cout << ans;
		if (ans != 0) cout << ' ' << b_min;
		cout << '\n';
	}
	return 0;
}
