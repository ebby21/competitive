/**
	question: uva.onlinejudge.org/external/113/11389.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, d, r, i, s;
	vector<int> a, e;
	while (cin >> n >> d >> r, n || d || r) {
		a.resize(n);
		e.resize(n);
		for (int &a_i: a) cin >> a_i;
		for (int &e_i: e) cin >> e_i;
		sort(a.begin(), a.end());
		sort(e.begin(), e.end(), greater<int>());
		s = 0;
		for (i = 0; i < n; ++i) // greedy load balancing
			s += max(a[i] + e[i] - d, 0);
		cout << s*r << '\n';
	}
	return 0;
}
