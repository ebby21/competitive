/**
	question: https://onlinejudge.org/external/112/11264.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int C[1000];
int main() {
	FastIO();
	int t, n, i, mc, s;
	cin >> t;
	while (t--) {
		cin >> n;
		for (i = 0; i < n; ++i) cin >> C[i];
		mc = 1;
		s = 0;
		for (i = 0; i < n - 1; ++i) {
			if (s + C[i] < C[i + 1]) {
				s += C[i];
				++mc;
			}
		}
		cout << mc << '\n';
	}
	return 0;
}
