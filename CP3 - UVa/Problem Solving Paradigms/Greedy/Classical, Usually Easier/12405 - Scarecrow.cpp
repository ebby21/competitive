/**
	question: https://onlinejudge.org/external/124/12405.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

char f[101];
int main() {
	FastIO();
	int t, _i, n, sc, i;
	cin >> t;
	for (_i = 1; _i <= t; ++_i) {
		cout << "Case " << _i << ": ";
		cin >> n >> f;
		sc = 0;
		for (i = 0; i < n; ++i) {
			if (f[i] == '.') {
				++sc;
				i += 2;
			}
		}
		cout << sc << '\n';
	}
	return 0;
}
