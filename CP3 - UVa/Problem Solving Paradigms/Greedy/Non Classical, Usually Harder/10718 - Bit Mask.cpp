/**
	question: https://onlinejudge.org/external/107/10718.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef unsigned int uint;

int main() {
	FastIO();
	uint n, l, u, m, i;
	bool lt, gt; // less than u, grea. than l
	while (cin >> n >> l >> u) {
		m = 0;
		for (i = (1<<31); i > 0; i >>= 1) {
			if ((l & i) != (u & i))
				break;
			m |= (l & i);
		}
		gt = lt = false;
		while (i > 0) {
			if (!(n & i)) {
				if (lt || (u & i))
					m |= i;
			}
			if (!gt && (l & i)) m |= i;
			if ((m & i) && !(l & i)) gt = true;
			if (!(m & i) && (u & i)) lt = true;
			i >>= 1;
		}
		cout << m << '\n';
	}
	return 0;
}
