/**
	question: https://onlinejudge.org/external/106/10656.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int i, n, x;
	bool ns, pr;
	while (cin >> n, n) {
		ns = pr = false;
		for (i = 0; i < n; ++i) {
			cin >> x;
			if (x > 0) {
				if (ns) cout << ' '; else ns = true;
				cout << x;
				pr = true;
			}
		}
		if (n > 0 && !pr) cout << 0;
		cout << '\n';
	}
	return 0;
}
