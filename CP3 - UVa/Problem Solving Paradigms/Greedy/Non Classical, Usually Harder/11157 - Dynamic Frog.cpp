/**
	question: https://onlinejudge.org/external/111/11157.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

using namespace std;

typedef pair<char, int> ci;

const int N = 100;
ci S[N+1];

int main() {
	int t, n, d, i, j, mlp, lp, start;
	vector<int> small;
	cin >> t;
	for (int _i = 1; _i <= t; ++_i) {
		cout << "Case " << _i << ": ";
		cin >> n >> d;
		for (i = 0; i < n; ++i)
			scanf(" %c-%d", &S[i].first, &S[i].second);
		S[n] = {'B', d};
		lp = 0;
		mlp = 0;
		start = 0;
		small.clear();
		for (i = 0; i < n + 1; ++i) {
			if (S[i].first == 'B') {
				if (small.size() > 1) {
					lp = max(small[0] - start, small[1] - start);
					for (j = 2; j < small.size(); j += 2)
						lp = max(lp, small[j] - small[j - 2]);
					lp = max(lp, S[i].second - small[j - 2]);
					for (j = 3; j < small.size(); j += 2)
						lp = max(lp, small[j] - small[j - 2]);
					lp = max(lp, S[i].second - small[j - 2]);
				} else {
					lp = S[i].second - start;
				}
				start = S[i].second;
				mlp = max(lp, mlp);
				small.clear();
			} else {
				small.push_back(S[i].second);
			}
		}
		cout << mlp << '\n';
	}
	return 0;
}
