/**
	question: https://onlinejudge.org/external/114/11456.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int tc, n, i, j, ans;
	vector<int> c, lis, lds;
	cin >> tc;
	while (tc--) {
		cin >> n;
		c.resize(n);
		lis.resize(n);
		lds.resize(n);
		for (int &c_i: c) cin >> c_i;
		ans = 0;
		for (i = n - 1; i >= 0; --i) {
			lis[i] = lds[i] = 1;
			for (j = i + 1; j < n; ++j) {
				if (c[j] < c[i])
					lis[i] = max(lis[i], lis[j] + 1);
				else if (c[j] > c[i])
					lds[i] = max(lds[i], lds[j] + 1);
			}
			ans = max(ans, lis[i] + lds[i] - 1);
		}
		cout << ans << '\n';
	}
	return 0;
}
