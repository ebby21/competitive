/**
	question: https://onlinejudge.org/external/117/11790.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int tc, n, a, b, i, j;
	vector<int> h, w, lis, lds;
	cin >> tc;
	for (int _i = 1; _i <= tc; ++_i) {
		cout << "Case " << _i << ". ";

		cin >> n;
		h.resize(n);
		w.resize(n);
		lis.resize(n);
		lds.resize(n);

		for (int &h_i: h) cin >> h_i;
		for (int &w_i: w) cin >> w_i;

		a = b = 0;
		for (i = 0; i < n; ++i) {
			lis[i] = lds[i] = w[i];
			for (j = 0; j < i; ++j) {
				if (h[j] < h[i])
					lis[i] = max(lis[i], lis[j] + w[i]);
				else if (h[j] > h[i])
					lds[i] = max(lds[i], lds[j] + w[i]);
			}
			a = max(a, lis[i]);
			b = max(b, lds[i]);
		}

		if  (a >= b) cout << "Increasing (" << a << "). Decreasing (" << b << ").\n";
		else         cout << "Decreasing (" << b << "). Increasing (" << a << ").\n";
	}
	return 0;
}
