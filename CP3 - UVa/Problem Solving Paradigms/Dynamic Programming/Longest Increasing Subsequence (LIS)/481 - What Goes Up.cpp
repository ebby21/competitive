/**
	question: https://onlinejudge.org/external/4/481.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, int> ii;

int main() {
	FastIO();
	vector<int> a, ls, lid, prv;
	int x, m, n;
	while (cin >> x) a.push_back(x);
	m = 0;
	n = a.size();
	ls.resize(n);  // ls[i]  -> smallest ending point of (i + 1) sized sequence
	lid.resize(n); // lid[i] -> index of the same
	prv.resize(n); // prv[i] -> index of the element before this one in the sequence
	for (int i = 0, p; i < n; ++i) {
		// find the least far element which can come before this
		p = lower_bound(ls.begin(), ls.begin() + m, a[i]) - ls.begin(); // -> first element >= this
		ls[p] = a[i];
		lid[p] = i;
		prv[i] = p > 0? lid[p - 1]: -1;
		m = max(m, p + 1);
	}
	int e = lid[m - 1];
	ls.clear();
	while (e != -1) {
	// generate lis again
		ls.push_back(a[e]);
		e = prv[e];
	}
	cout << m;
	cout << "\n-\n";
	for (int i = m - 1; i >= 0; --i)
		cout << ls[i] << '\n';
	return 0;
}
