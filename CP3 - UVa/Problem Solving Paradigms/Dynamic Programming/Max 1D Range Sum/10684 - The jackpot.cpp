/**
	question: https://onlinejudge.org/external/106/10684.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, ans, sum, x;
	while (cin >> n, n) {
		ans = sum = 0;
		while (n--) {
			cin >> x;
			sum += x;
			ans = max(ans, sum);
			if (sum < 0) sum = 0;
		}
		if  (ans <= 0) cout << "Losing streak";
		else           cout << "The maximum winning streak is " << ans;
		cout << ".\n";
	}
	return 0;
}
