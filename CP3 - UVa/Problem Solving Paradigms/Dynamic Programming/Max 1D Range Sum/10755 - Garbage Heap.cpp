/**
	question: https://onlinejudge.org/external/107/10755.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef long long ll;

const int N = 20;
ll S[N][N][N];

void fill_table(int a, int b, int c) {
	int i, j, k;
	for (i = 0; i < a; ++i) for (j = 0; j < b; ++j) for (k = 0; k < c; ++k)
	{
		if (i > 0) S[i][j][k] += S[i - 1][j][k];
		if (j > 0) S[i][j][k] += S[i][j - 1][k];
		if (k > 0) S[i][j][k] += S[i][j][k - 1];

		if (i > 0 && j > 0) S[i][j][k] -= S[i - 1][j - 1][k];
		if (j > 0 && k > 0) S[i][j][k] -= S[i][j - 1][k - 1];
		if (k > 0 && i > 0) S[i][j][k] -= S[i - 1][j][k - 1];

		if (i > 0 && j > 0 && k > 0) S[i][j][k] += S[i - 1][j - 1][k - 1];
	}
}

int main() {
	FastIO();
	int t, a, b, c, i, j, k, l, m, n;
	ll ans, sum;
	cin >> t;
	while (t--) {
		cin >> a >> b >> c;
		for (i = 0; i < a; ++i)
		 for (j = 0; j < b; ++j)
		  for (k = 0; k < c; ++k)
		  	cin >> S[i][j][k];
		fill_table(a, b, c); // construct cumulative 3d matrix
		ans = S[0][0][0];
		for (i = 0; i < a; ++i) for (j = 0; j < b; ++j) for (k = 0; k < c; ++k) // beg
		for (l = i; l < a; ++l) for (m = j; m < b; ++m) for (n = k; n < c; ++n) // end
		{
			sum = S[l][m][n]; // find sum, inclusion-exclusion

			if (i > 0) sum -= S[i - 1][m][n];
			if (j > 0) sum -= S[l][j - 1][n];
			if (k > 0) sum -= S[l][m][k - 1];

			if (i > 0 && j > 0) sum += S[i - 1][j - 1][n];
			if (j > 0 && k > 0) sum += S[l][j - 1][k - 1];
			if (k > 0 && i > 0) sum += S[i - 1][m][k - 1];

			if (i > 0 && j > 0 && k > 0) sum -= S[i - 1][j - 1][k - 1];

			if (sum > ans) ans = sum;
		}
		cout << ans << '\n';
		if (t != 0) cout << '\n';
	}
	return 0;
}
