/**
	question: https://onlinejudge.org/external/7/787.pdf

	language: Java
	author  : ebby21
**/

import java.io.*;
import java.util.StringTokenizer;
import java.math.BigInteger;

class Main {
	static String readLn (int maxLg) {
	//	utility function to read from stdin
		byte lin[] = new byte [maxLg];
		int lg = 0, car = -1;

		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n')) break;
				lin [lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0)) return (null);  // eof
		return (new String (lin, 0, lg));
	}

	public static void main (String args[]) {
		Main solution = new Main();
		solution.begin();
	}

	void begin() {
		String input;
		StringTokenizer idata;
		BigInteger ans;
		BigInteger dp[][];
		/* dp[i][j] -> product of 'j + 1' integers from ith pos */
		int val[] = new int[100];
		int n, x, i, j;
		while ((input = Main.readLn(1023)) != null) {
			idata = new StringTokenizer(input);
			n = 0;
			while (true) {
				x = Integer.parseInt(idata.nextToken());
				if (x == -999999) break;
				val[n++] = x; // read all values into val[]
			}
			dp = new BigInteger[n][n];
			dp[0][0] = BigInteger.valueOf(val[0]);
			for (i = 0; i < n; ++i) {
				dp[i][0] = BigInteger.valueOf(val[i]);
				for (j = 1; j < n - i; ++j)
					dp[i][j] = dp[i][j - 1].multiply(BigInteger.valueOf(val[i + j]));
			}
			ans = dp[0][0];
			for (i = 0; i < n; ++i) {
				for (j = 0; j < n - i; ++j) {
					if (ans.compareTo(dp[i][j]) == -1)
						ans = dp[i][j];
				}
			}
			System.out.println(ans);
		}
	}
}
