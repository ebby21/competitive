/**
	question: https://onlinejudge.org/external/1/108.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int N = 100;
int A[N][N];

void fill_table(int n) {
	int i, j;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			if (i > 0) A[i][j] += A[i - 1][j];
			if (j > 0) A[i][j] += A[i][j - 1];
			if (i > 0 && j > 0) A[i][j] -= A[i - 1][j - 1];
		}
	}
}

int main() {
	FastIO();
	int n, i, j, k, l;
	cin >> n;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j)
			cin >> A[i][j];
	}
	fill_table(n);
	int ms=int(-1e9), s;
	for (i = 0; i < n; ++i) for (j = 0; j < n; ++j) // beg
	for (k = i; k < n; ++k) for (l = j; l < n; ++l) // end
	{
		s = A[k][l];
		if (i > 0) s -= A[i - 1][l];
		if (j > 0) s -= A[k][j - 1];
		if (i > 0 && j > 0) s += A[i - 1][j - 1];
		if (s > ms) ms = s;
	}
	cout << ms << '\n';
	return 0;
}
