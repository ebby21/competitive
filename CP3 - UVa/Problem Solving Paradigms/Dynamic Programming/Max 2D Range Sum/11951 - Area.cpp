/**
	question: https://onlinejudge.org/external/119/11951.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef long long ll;

const int N = 100;

ll P[N][N];

void fill_table(int n, int m) {
	int i, j;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < m; ++j) {
			if (i > 0) P[i][j] += P[i - 1][j];
			if (j > 0) P[i][j] += P[i][j - 1];
			if (i > 0 && j > 0) P[i][j] -= P[i - 1][j - 1];
		}
	}
}

ll get_cost(int i, int j, int k, int l) {
	ll s = P[k][l];
	if (i > 0) s -= P[i - 1][l];
	if (j > 0) s -= P[k][j - 1];
	if (i > 0 && j > 0) s += P[i - 1][j - 1];
	return s;
}

int main() {
	FastIO();
	int t, n, m, i, j, a, x, y, ma;
	ll k, mc, c;
	cin >> t;
	for (int _i = 1; _i <= t; ++_i) {
		cout << "Case #" << _i << ": ";
		cin >> n >> m >> k;
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j)
				cin >> P[i][j];
		}
		fill_table(n, m);
		mc = ma = 0;
		for (i = 0; i < n; ++i) for (j = 0; j < m; ++j) {
			for (x = i; x < n; ++x)
			for (y = max(0, j - 1 + ma/(x - i + 1)); y < m; ++y) {
			// slight pruning -> area >= ma
				c = get_cost(i, j, x, y);
				a = (x - i + 1)*(y - j + 1);
				if (c <= k) {
					if (a > ma || (a == ma && c < mc)) {
						ma = a;
						mc = c;
					}
				}
			}
		}
		cout << ma << ' ' << mc << '\n';
	}
	return 0;
}
