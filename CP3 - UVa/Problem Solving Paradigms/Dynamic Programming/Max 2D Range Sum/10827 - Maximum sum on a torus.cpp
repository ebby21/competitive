/**
	question: https://onlinejudge.org/external/108/10827.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int N = 75*2;

int A[N][N];

void fill_table(int n) {
	int i, j;
	for (i = 0; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			if (i > 0) A[i][j] += A[i - 1][j];
			if (j > 0) A[i][j] += A[i][j - 1];
			if (i > 0 && j > 0) A[i][j] -= A[i - 1][j - 1];
		}
	}
}

int get_sum(int i, int j, int k, int l) {
	int s = A[k][l];
	if (i > 0) s -= A[i - 1][l];
	if (j > 0) s -= A[k][j - 1];
	if (i > 0 && j > 0) s += A[i - 1][j - 1];
	return s;
}

int main() {
	FastIO();
	int t, n, i, j, k, l, ms, s;
	cin >> t;
	while (t--) {
		cin >> n;
		for (i = 0; i < n; ++i) {
			for (j = 0; j < n; ++j) {
				cin >> A[i][j];
				A[n + i][j] = A[i][n + j] =
				A[n + i][n + j] = A[i][j];
			}
		}
		fill_table(2*n);
		ms=int(-1e9);
		for (i = 0; i < n; ++i) for (j = 0; j < n; ++j) {
			for (k = i; k < i + n; ++k) for (l = j; l < j + n; ++l) {
				s = get_sum(i, j, k, l);
				if (s > ms) ms = s;
			}
		}
		cout << ms << '\n';
	}
	return 0;
}
