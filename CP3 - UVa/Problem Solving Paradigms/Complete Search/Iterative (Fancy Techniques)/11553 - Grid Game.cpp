/**
	question: https://onlinejudge.org/external/115/11553.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <numeric>
#include <climits>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int t, n, grid[8][8], i, j, p[8], ms, s;
	cin >> t;
	iota(p, p + 8, 0);
	while (t--) {
		cin >> n;
		for (i = 0; i < n; ++i) {
			for (j = 0; j < n; ++j)
				cin >> grid[i][j];
		}
		ms = INT_MAX;
		do {
			s = 0;
			for (i = 0; i < n; ++i)
				s += grid[i][p[i]];
			ms = min(ms, s);
		} while (next_permutation(p, p + n));
		cout << ms << '\n';
	}
	return 0;
}
