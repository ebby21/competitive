/**
	question: https://onlinejudge.org/external/10/1047.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <climits>
#include <algorithm>
#include <vector>
#include <set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int _i = 1, n, t, m, k, i, md, d, j;
	vector<bool> p;
	vector<int> a;
	set<int> r, ans;
	vector<pair<set<int>, int>> common;
	while (cin >> n >> t, n != 0 || t != 0) {
		for (auto &x: common)
			x.first.clear();
		a.resize(n);
		for (int &x: a) cin >> x;
		cin >> m;
		common.resize(m);
		for (auto &x: common) {
			cin >> k;
			while (k--) {
				cin >> d;
				x.first.insert(d);
			}
			cin >> x.second;
		}
		p.assign(n, false);
		fill(p.begin(), p.begin() + t, true);
		md = INT_MIN;
		do {
			r.clear();
			d = 0;
			for (i = 0; i < n; ++i) {
				if (p[i]) {
					r.insert(i + 1);
					d += a[i];
				}
			}
			for (auto &x: common) {
				j = 0;
				for (int y: x.first) {
					if (r.find(y) != r.end())
						++j;
				}
				if (j > 1) d -= (j - 1)*x.second; // this was added j times, only 1 was neede
			}
			if (d > md) {
				md = d;
				ans = r;
			}
		} while (prev_permutation(p.begin(), p.end())); // generate all combinations
		cout << "Case Number  " << _i++ << '\n'
		     << "Number of Customers: " << md << '\n'
		     << "Locations recommended:";
		for (int x: ans) cout << ' ' << x;
		cout << "\n\n";
	}
	return 0;
}
