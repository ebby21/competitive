/**
	question: https://onlinejudge.org/external/124/12455.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int t, n, p, i, j, sum;
	bool found;
	vector<int> b;
	cin >> t;
	while (t--) {
		cin >> n >> p;
		b.resize(p);
		for (int &x: b) cin >> x;
		found = false;
		for (i = 0; i < (1<<p); ++i) {
			sum = 0;
			for (j = 0; j < p; ++j) {
				if (i & (1<<j)) // if j-th bit is set, we take it into account
					sum += b[j];
			}
			if (sum == n) {
				found = true;
				break;
			}
		}
		cout << (found? "YES": "NO") << '\n';
	}
	return 0;
}
