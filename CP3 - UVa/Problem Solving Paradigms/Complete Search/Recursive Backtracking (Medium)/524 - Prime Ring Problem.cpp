/**
	question: https://onlinejudge.org/external/5/524.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>
#include <set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

bool Prime(int n) {
	if (n < 2) return false;
	int p, hi = sqrt(n);
	for (p = 2; p <= hi; ++p) {
		if (n % p == 0)
			return false;
	}
	return true;
}

int p[16] = {1};
void prime_rings(int n, int i=1, int t=1) {
	if (i >= n && Prime(p[0] + p[n - 1])) {
		cout << p[0];
		for (i = 1; i < n; ++i)
			cout << ' ' << p[i];
		cout << '\n';
		return;
	}
	for (int j = 0; j < n; ++j) {
		if (!(t & (1<<j))) {
			p[i] = j + 1;
			if (!Prime(p[i - 1] + p[i])) continue;
			prime_rings(n, i + 1, t | (1<<j));
		}
	}
}

int main() {
	FastIO();
	int _i = 1, n;
	while (cin >> n) {
		if (_i > 1) cout << '\n';
		cout << "Case " << _i++ << ":\n";
		prime_rings(n);
	}
	return 0;
}
