/**
	question: https://onlinejudge.org/external/5/574.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int x[12];
set<string> soln; // to filter unique soln.s

void sums_of(int t, int n, int i=0, int nos=0, int s=0) {
	if (i == n) {
		if (s != t) return;
		bool first = true;
		string ans = "";
		for (i = 0; i < n; ++i) {
			if (nos & (1<<i)) {
				if (first) {
					ans += to_string(x[i]);
					first = false;
				} else {
					ans += "+" + to_string(x[i]);
				}
			}
		}
		soln.insert(ans);
		return;
	}
	if (i == 0) soln.clear();
	if (s + x[i] < t)
		sums_of(t, n, i + 1, nos | (1<<i), s + x[i]);
	else if (s + x[i] == t)
		sums_of(t, n, n, nos | (1<<i), s + x[i]);

	sums_of(t, n, i + 1, nos, s);
}

int main() {
	FastIO();
	int t, n, i;
	while (cin >> t >> n, n != 0) {
		for (i = 0; i < n; ++i)
			cin >> x[i];
		cout << "Sums of " << t << ":\n";
		sums_of(t, n);
		if (soln.empty()) {
			cout << "NONE\n";
		} else {
			for (auto it = soln.rbegin(); it != soln.rend(); ++it) // nonincreasing order
				cout << *it << '\n';
		}
	}
	return 0;
}
