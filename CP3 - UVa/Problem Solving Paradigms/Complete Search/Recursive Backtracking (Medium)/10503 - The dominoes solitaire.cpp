/**
	question: https://onlinejudge.org/external/105/10503.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, int> ii;

ii p[14];
int b, d;

bool possible(int n, int m, int c=b, int t=0) {
	if (__builtin_popcount(t) == n) // no. of set bits
		return c == d;
	for (int j = 0; j < m; ++j) {
		if (!(t & (1<<j))) {
			if (p[j].first == c) {
				if (possible(n, m, p[j].second, t | (1<<j)))
					return true;
			} else if (p[j].second == c) {
				if (possible(n, m, p[j].first, t | (1<<j)))
					return true;
			}
		}
	}
	return false;
}

int main() {
	FastIO();
	int n, m, i;
	while (cin >> n, n != 0) {
		cin >> m >> i >> b >> d >> i;
		for (i = 0; i < m; ++i)
			cin >> p[i].first >> p[i].second;
		cout <<  (possible(n, m) ? "YES": "NO") << '\n';
	}
	return 0;
}
