/**
	question: https://onlinejudge.org/external/112/11242.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int f, r;
	vector<int> fc, rc;
	vector<double> d;
	double s, ms;
	while (cin >> f, f != 0) {
		cin >> r;
		fc.resize(f);
		rc.resize(r);
		for (int &x: fc) cin >> x;
		for (int &x: rc) cin >> x;
		for (int m: fc) {
			for (int n: rc)
				d.push_back(n/double(m));
		}
		sort(d.begin(), d.end());
		ms = 0.0;
		for (f = 1; f < int(d.size()); ++f) {
			if (d[f - 1] == 0) continue;
			s = d[f]/d[f - 1];
			if (s > ms) ms = s;
		}
		d.clear();
		cout << fixed << setprecision(2) << ms << '\n';
	}
	return 0;
}
