/**
	question: https://onlinejudge.org/external/104/10487.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <climits>
#include <algorithm>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int _i = 1, n, m, q, i, j, d, md, ans;
	vector<int> a;
	while (cin >> n, n != 0) {
		cout << "Case " << _i++ << ":\n";
		a.resize(n);
		for (int &x: a) cin >> x;
		sort(a.begin(), a.end());
		cin >> m;
		while (m--) {
			cin >> q;
			md = INT_MAX;
			for (i = 0; i < n; ++i) {
				for (j = i + 1; j < n; ++j) {
					d = abs(a[i] + a[j] - q);
					if (d < md) {
						ans = a[i] + a[j];
						md = d;
					}
					if (a[i] + a[j] > q) break; // slight pruning
				}
			}
			cout << "Closest sum to " << q << " is " << ans << ".\n";
		}
	}
	return 0;
}
