/**
	question: https://onlinejudge.org/external/12/1260.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int T, n, i, j;
	vector<int> a;
	int sum;
	cin >> T;
	while (T--) {
		cin >> n;
		a.resize(n);
		for (int &x: a) cin >> x;
		sum = 0;
		for (i = 1; i < n; ++i) {
			for (j = 0; j < i; ++j) {
				if (a[j] <= a[i])
					++sum;
			}
		}
		cout << sum << '\n';
	}
	return 0;
}
