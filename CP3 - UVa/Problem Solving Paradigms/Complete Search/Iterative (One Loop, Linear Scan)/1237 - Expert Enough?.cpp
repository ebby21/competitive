/**
	question: https://onlinejudge.org/external/12/1237.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef struct {
	char name[21];
	int lo, hi;
} car;

array<car, int(1e4)> m;

bool in_range(int x, int lo, int hi) { return lo <= x && x <= hi; }

int search(int d, int p) {
	int ans = -1;
	for (int i = 0; i < d; ++i) {
		if (in_range(p, m[i].lo, m[i].hi)) {
			if (ans != -1) return -1;
			ans = i;
		}
	}
	return ans;
}

int main() {
	FastIO();
	int T, d, q, p, i;
	cin >> T;
	while (T--) {
		cin >> d;
		for (i = 0; i < d; ++i)
			cin >> m[i].name >> m[i].lo >> m[i].hi;
		cin >> q;
		while (q--) {
			cin >> p;
			i = search(d, p);
			cout << (i == -1? "UNDETERMINED": m[i].name) << '\n';
		}
		if (T != 0) cout << '\n';
	}
	return 0;
}
