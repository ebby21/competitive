/**
	question: https://onlinejudge.org/external/9/927.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int C, c[21], i, k, d, j, n;
	long long t, p;
	cin >> C;
	while (C--) {
		cin >> i;
		for (j = 0; j <= i; ++j)
			cin >> c[j];
		cin >> d >> k;
		n = 1;
		while (k > d*n) {
			k -= d*n;
			++n;
		}
		t = c[0];
		p = 1;
		for (j = 1; j <= i; ++j) {
			p *= n;
			t += c[j]*p;
		}
		cout << t << '\n';
	}
	return 0;
}
