/**
	question: https://onlinejudge.org/external/109/10976.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, int> ii;

int main() {
	FastIO();
	int k, x;
	vector<ii> ans;
	while (cin >> k) {
		for (x = k + 1; x <= 2*k; ++x) {
			if (k*x % (x - k) == 0)
				ans.push_back({k*x/(x - k), x});
		}
		cout << ans.size() << '\n';
		for (ii &c: ans)
			cout << "1/" << k << " = " << "1/" << c.first << " + " << "1/" << c.second << '\n';
		ans.clear();
	}
	return 0;
}
