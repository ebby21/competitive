/**
	question: https://onlinejudge.org/external/106/10660.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <climits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int p[25], a[5], md[25];

int id(int i, int j) { return 5*i + j; }

int dist(int i, int j) { return abs(i/5 - j/5) + abs(i%5 - j%5); }

void solve() {
	int i, j, k, l, m, n, msum = INT_MAX, sum;
	for (i =    0; i < 21; ++i) {
	 for (j = i + 1; j < 22; ++j) {
	  for (k = j + 1; k < 23; ++k) {
	   for (l = k + 1; l < 24; ++l) {
	    for (m = l + 1; m < 25; ++m) {
	     sum = 0;
	     for (n = 0; n < 25; ++n)
	         sum += p[n]*min({dist(i,n), dist(j,n), dist(k,n), dist(l,n), dist(m,n)});
	     if (sum < msum) {
	         msum = sum;
	         a[0] = i, a[1] = j, a[2] = k, a[3] = l, a[4] = m;
	     }
	    }
	   }
	  }
	 }
	}
}

int main() {
	FastIO();
	int t, n, x, y, c;
	cin >> t;
	while (t--) {
		cin >> n;
		fill(p, p + 25, 0);
		while (n--) {
			cin >> x >> y >> c;
			p[id(x, y)] = c;
		}
		solve();
		for (x = 0; x < 5; ++x) {
			if (x > 0) cout << ' ';
			cout << a[x];
		}
		cout << '\n';
	}
	return 0;
}
