/**
	question: https://onlinejudge.org/external/112/11236.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	cout << fixed << setprecision(2);
	const int P = 2000, E6 = int(1e6);
	int a, b, c, d;
	long long p, s;
	for (a = 1; a < P; ++a) {
		for (b = a; a + b < P; ++b) {
			for (c = b; a + b + c < P; ++c) {
				p = E6*(a + b + c);
				s = 1LL*a*b*c - E6;
				if (s == 0) continue;
				if (p % s == 0) {
					d = p/s;
					if (d >= c) {
						s = a + b + c + d;
						p = 1LL*a*b*c*d;
						if (s <= P && E6*s == p)
							cout << a/1e2 << ' ' << b/1e2 << ' ' << c/1e2 << ' ' << d/1e2 << '\n';
					}
				}
			}
		}
	}
	return 0;
}
