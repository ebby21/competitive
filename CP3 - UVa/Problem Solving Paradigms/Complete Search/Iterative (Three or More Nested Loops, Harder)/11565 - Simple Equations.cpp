/**
	question: https://onlinejudge.org/external/115/11565.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, a, b, c, x, y, z, z2;
	bool found;
	cin >> n;
	while (n--) {
		cin >> a >> b >> c;
		found = false;
		for (x = -21; x <= 21; ++x) { // cbrt(1e4)
			for (y = -100; y <= 100; ++y) {
				if (x == y) continue;
				z2 = c - x*x - y*y;
				if (z2 >= 0) {
					z = sqrt(z2);
					if (z == x || z == y) continue;
					if (z*z == z2 && (x + y + z == a) && (x*y*z == b)) {
						found = true;
						break;
					}
				}
			}
			if (found) break;
		}
		if  (found) cout << x << ' ' << y << ' ' << z << '\n';
		else        cout << "No solution.\n";
	}
	return 0;
}
