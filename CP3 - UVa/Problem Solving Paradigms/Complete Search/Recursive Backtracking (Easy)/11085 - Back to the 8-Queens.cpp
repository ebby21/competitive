/**
	question: https://onlinejudge.org/external/110/11085.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int q8[92][8], q[8], i, co = 0, j;

void generate_8_queens(int c) {
	if (c == 8) {
		for (i = 0; i < 8; ++i)
			q8[co][i] = q[i] + 1;
		++co;
		return;
	}
	bool att;
	for (int r = 0; r < 8; ++r) {
		att = false;
		for (i = 0; i < c; ++i) {
			if (q[i] == r || abs(q[i] - r) == abs(i - c)) {
				att = true;
				break;
			}
		}
		if (att) continue;
		q[c] = r;
		generate_8_queens(c + 1);
	}
}

int main() {
	FastIO();
	generate_8_queens(0);
	int s, ms, _i = 1;
	while (cin >> q[0]) {
		for (i = 1; i < 8; ++i) cin >> q[i];
		ms = 0;
		for (i = 0; i < 92; ++i) {
			s = 0;
			for (j = 0; j < 8; ++j) {
				if (q8[i][j] == q[j])
					++s;
			}
			ms = max(ms, s);
		}
		cout << "Case " << _i++ << ": " <<  8 - ms << '\n';
	}
	return 0;
}
