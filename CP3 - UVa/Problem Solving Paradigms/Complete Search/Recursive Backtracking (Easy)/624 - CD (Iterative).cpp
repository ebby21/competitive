/**
	question: https://onlinejudge.org/external/6/624.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, t, i, j, bs, s, tracks;
	vector<int> a;
	while (cin >> n >> t) {
		a.resize(t);
		for (int &x: a) cin >> x;
		bs = n;
		for (i = 1; i < (1 << t); ++i) {
			s = 0;
			for (j = 0; j < t; ++j) {
				if (i & (1 << j))
					s += a[j];
			}
			if (s <= n && n - s < bs) {
				bs = n - s;
				tracks = i;
			}
		}
		s = 0;
		for (j = 0; j < t; ++j) {
			if (tracks & (1 << j)) {
				s += a[j];
				cout << a[j] << ' ';
			}
		}
		cout << "sum:" << s << '\n';
	}
	return 0;
}
