/**
	question: https://onlinejudge.org/external/105/10576.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int d[2], i, j, k, s, ms;
	bool satisfy;
	while (cin >> d[0] >> d[1]) {
		ms = -1;
		d[1] = -d[1];
		for (i = 0; i < (1<<12); ++i) { // all possiblilities (0->s, 1->d)
			s = 0;
			for (j = 0; j < 4; ++j)
				s += d[(i>>j) & 1];
			k = 0;
			satisfy = true;
			while (j < 12) {
				if (s + d[(i>>j) & 1] > 0) {
					satisfy = false;
					break;
				}
				s = s - d[(i>>k) & 1] + d[(i>>j) & 1];
				++j, ++k;
			}
			if (satisfy) {
				s = 0;
				for (j = 0; j < 12; ++j)
					s += d[(i>>j) & 1];
				ms = max(s, ms);
			}
		}
		if  (ms < 0) cout << "Deficit\n";
		else         cout << ms << '\n';
	}
	return 0;
}
