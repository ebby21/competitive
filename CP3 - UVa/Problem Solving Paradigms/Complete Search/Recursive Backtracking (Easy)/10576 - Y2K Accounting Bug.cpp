/**
	question: https://onlinejudge.org/external/105/10576.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <climits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int max_surplus(int s, int d, int i=0, int p4=0, int st=0) {
	if (i == 12) return 0;
	bool sur = true, def = true;
	if (i >= 4) {
		if (p4 + s > 0) sur = false;
		if (p4 - d > 0) def = false;
		if  (st & (1<<3)) p4 -= s;
		else              p4 += d;
	}
	int surplus = INT_MIN;
	if (sur) surplus = max(surplus, max_surplus(s, d, i + 1, p4 + s, (st<<1) + 1) + s);
	if (def) surplus = max(surplus, max_surplus(s, d, i + 1, p4 - d, (st<<1) + 0) - d);
	return surplus;
}

int main() {
	FastIO();
	int s, d, a;
	while (cin >> s >> d) {
		a = max_surplus(s, d);
		if  (a < 0) cout << "Deficit\n";
		else        cout << a << '\n';
	}
	return 0;
}
