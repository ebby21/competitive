/**
	question: https://onlinejudge.org/external/6/624.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, int> ii;
typedef vector<int> vi;

ii best_use(int n, vi &a, int t=0, int i=0) {
	if (i >= int(a.size()) || n == 0)
		return {t, n};
	ii ans = best_use(n, a, t, i + 1);
	if (a[i] <= n) {
		ii other = best_use(n - a[i], a, t | (1 << i), i + 1);
		if (other.second < ans.second)
			ans = other;
	}
	return ans;
}

int main() {
	FastIO();
	int n, t, i;
	vi a;
	ii ans;
	while (cin >> n >> t) {
		a.resize(t);
		for (int &x: a) cin >> x;
		ans = best_use(n, a);
		for (i = 0; i < t; ++i) {
			if (ans.first & (1 << i))
				cout << a[i] << ' ';
		}
		cout << "sum:" << n - ans.second << '\n';
	}
	return 0;
}
