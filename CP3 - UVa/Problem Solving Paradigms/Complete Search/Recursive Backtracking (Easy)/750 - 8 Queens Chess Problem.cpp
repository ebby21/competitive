/**
	question: https://onlinejudge.org/external/7/750.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int q[8], s_no, i;

void print_8_queens(int c, int qr, int qc) {
	if (c == 8) {
		cout << setw(2) << ++s_no << "     "; // setw is not sticky
		for (i = 0; i < 8; ++i)
			cout << ' ' << q[i] + 1;
		cout << '\n';
		return;
	}
	bool att;
	for (int r = 0; r < 8; ++r) {
		if ((r == qr) != (c == qc)) continue; // if one true, other false
		att = false;
		for (i = 0; i < c; ++i) {
			if (q[i] == r || abs(q[i] - r) == abs(i - c)) {
				att = true;
				break;
			}
		}
		if (att) continue;
		q[c] = r;
		print_8_queens(c + 1, qr, qc);
	}
}

void print_8_queens(int qr, int qc) {
	s_no = 0;
	print_8_queens(0, qr - 1, qc - 1);
}

int main() {
	FastIO();
	int t, r, c;
	cin >> t;
	while (t--) {
		cin >> r >> c;
		cout << "SOLN       COLUMN\n";
		cout << " #      1 2 3 4 5 6 7 8\n\n";
		print_8_queens(r, c);
		if (t != 0) cout << '\n';
	}
	return 0;
}
