/**
	question: https://onlinejudge.org/external/7/735.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

// generate this array using set<int>
const int V = 43;
array<int, V> v = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 26, 27, 28, 30, 32, 33, 34, 36, 38, 39, 40, 42, 45, 48, 50, 51, 54, 57, 60};

int main() {
	FastIO();
	int n, i, j, k, C, P;
	while (cin >> n, n > 0) {
		C = P = 0;
		for (i = 0; i < V; ++i) {
			for (j = i; j < V; ++j) {
				for (k = j; k < V; ++k) {
					if (v[i] + v[j] + v[k] == n) {
						C += 1;
						if (v[i] == v[j] && v[j] == v[k])
							P += 1; // 3!/3!
						else if (v[i] == v[j] || v[j] == v[k] || v[i] == v[k])
							P += 3; // 3!/2!
						else
							P += 6; // 3!/0!
					}
				}
			}
		}
		if (C == 0) {
			cout << "THE SCORE OF " << n << " CANNOT BE MADE WITH THREE DARTS.\n";
		} else {
			cout << "NUMBER OF COMBINATIONS THAT SCORES " << n << " IS " << C << ".\n";
			cout << "NUMBER OF PERMUTATIONS THAT SCORES " << n << " IS " << P << ".\n";
		}
		cout << "**********************************************************************\n";
	}
	cout << "END OF OUTPUT\n";
	return 0;
}
