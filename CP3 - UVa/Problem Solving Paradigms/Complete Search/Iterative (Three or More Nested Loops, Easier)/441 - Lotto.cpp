/**
	question: https://onlinejudge.org/external/4/441.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int K, i, j, k, l, m, n;
	vector<int> s;
	cin >> K;
	while (K != 0) {
		s.resize(K);
		for (int &x: s) cin >> x;
		for (i = 0    ; i < K - 5; ++i)
		 for (j = i + 1; j < K - 4; ++j)
		  for (k = j + 1; k < K - 3; ++k)
		   for (l = k + 1; l < K - 2; ++l)
		    for (m = l + 1; m < K - 1; ++m)
		     for (n = m + 1; n < K    ; ++n)
		      cout << s[i] << ' ' << s[j] << ' ' << s[k] << ' ' << s[l] << ' ' << s[m] << ' ' << s[n] << '\n';
		cin >> K;
		if (K != 0) cout << '\n';
	}
	return 0;
}
