/**
	question: https://onlinejudge.org/external/101/10102.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <climits>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef vector<char> vc;
typedef pair<int, int> ii;

int main() {
	FastIO();
	int m, i, j, k, l, mmd, md;
	vector<vc> grid;
	while (cin >> m) {
		grid.assign(m, vc(m));
		for (i = 0; i < m; ++i) {
			for (j = 0; j < m; ++j)
				cin >> grid[i][j];
		}
		mmd = INT_MIN;
		for (i = 0; i < m; ++i) {
			for (j = 0; j < m; ++j) {
				if (grid[i][j] == '1') {
					md = INT_MAX;
					for (k = 0; k < m; ++k) {
						for (l = 0; l < m; ++l) {
							if (grid[k][l] == '3') {
								md = min(md, abs(k - i) + abs(l - j));
							}
						}
					}
					mmd = max(md, mmd);
				}
			}
		}
		cout << mmd << '\n';
		for (vc &row: grid) row.clear();
		grid.clear();
	}
	return 0;
}
