/**
	question: https://onlinejudge.org/external/1/193.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <bitset>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int Max = 100;

vector<int> graph[Max];
bitset<Max> c, best; // set -> Black
int mb;

void find_max_black(int n, int i=0) {
	if (i == n) {
		int b = c.count();
		if (b > mb) {
			mb = b;
			best = c;
		}
		return;
	}
	bool black = false;
	for (int j: graph[i]) {
		if (c[j]) {
			black = true;
			break;
		}
	}
	if (!black) {
		c[i] = true;
		find_max_black(n, i + 1);
	}
	c[i] = false;
	find_max_black(n, i + 1);
}

int main() {
	FastIO();
	int m, n, k, i, x, y;
	bool first;
	cin >> m;
	while (m--) {
		cin >> n >> k;
		for (i = 0; i < n; ++i)
			graph[i].clear();
		while (k--) {
			cin >> x >> y;
			--x, --y;
			graph[x].push_back(y);
			graph[y].push_back(x);
		}
		c.reset();
		mb = 0;
		find_max_black(n);
		cout << mb << '\n';
		first = true;
		for (i = 0; i < n; ++i) {
			if (best[i]) {
				if  (!first) cout << ' ';
				else         first = false;
				cout << i + 1;
			}
		}
		cout << '\n';
	}
	return 0;
}
