/**
	question: https://onlinejudge.org/external/12/1262.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef char grid[6][6];

grid one, two;
vector<char> p[5];
int n[5];

int main() {
	FastIO();
	int t, K, i, j, k, M;
	cin >> t;
	while (t--) {
		cin >> K;
		for (i = 0; i < 6; ++i)
			cin >> one[i];
		for (i = 0; i < 6; ++i)
			cin >> two[i];

		for (i = 0; i < 5; ++i) {
			p[i].clear();
			for (j = 0; j < 6; ++j) {
				if (find(p[i].begin(), p[i].end(), one[j][i]) != p[i].end())
				// unique passwords
					continue;
				for (k = 0; k < 6; ++k) {
					if (one[j][i] == two[k][i]) {
						p[i].push_back(one[j][i]);
						break;
					}
				}
			}
			sort(p[i].begin(), p[i].end());
		}

		M = 1;
		for (i = 0; i < 5; ++i)
			M *= p[i].size();

		if (M < K) {
			cout << "NO\n";
			continue;
		}

		fill(n, n + 5, 0);
		while (--K) {
			i = 4;
			while (true) {
				if (n[i] + 1 == int(p[i].size())) {
					n[i] = 0;
					--i;
				} else {
					++n[i];
					break;
				}
			}
		}

		for (i = 0; i < 5; ++i)
			cout << p[i][n[i]];
		cout << '\n';
	}
	return 0;
}
