/**
	question: https://onlinejudge.org/external/4/416.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <bitset>
#include <string>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef bitset<7> b7;

b7 digit[10] = {
	b7("1111110"),
	b7("0110000"),
	b7("1101101"),
	b7("1111001"),
	b7("0110011"),
	b7("1011011"),
	b7("1011111"),
	b7("1110000"),
	b7("1111111"),
	b7("1111011")
}, count[10];

// unset all bits in x that are set in b
b7 unset_set(b7 x, b7 b) { return ~((~x) | b); }

bool can_be_down_count(int n, int i=0, int d=9, b7 burnt=b7("0000000")) {
	if (i == n) return true;
	b7 possible = unset_set(count[i], digit[d]),
	   revived = (burnt & count[i]); // if any burnt are lit now
	if (possible.none() && revived.none()) {
		if (can_be_down_count(n, i + 1, d - 1, burnt | unset_set(digit[d], count[i])))
			return true;
	}

	if (i > 0) return false; // for i = 0, you can just vary d

	if (d > n - 1 && can_be_down_count(n, i, d - 1, burnt))
		return true;
	return false;
}

int main() {
	FastIO();
	int n, i;
	string led;
	while (cin >> n, n != 0) {
		for (i = 0; i < n; ++i) {
			cin >> led;
			for (char &c: led)
				c = (c == 'Y'? '1': '0');
			count[i] = b7(led);
		}
		cout << (can_be_down_count(n)? "MATCH": "MISMATCH") << '\n';
	}
	return 0;
}
