/**
	question: https://onlinejudge.org/external/121/12192.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int h[500][500], n, m, L, U;

bool side_possible(int s) {
	int i, j, lo, hi;
	for (i = 0; i <= n - s; ++i) {
		lo = lower_bound(h[i], h[i] + m, L) - h[i];
		hi = upper_bound(h[i], h[i] + m, U) - h[i];
		for (j = lo; j <= hi - s; ++j) {
			if (L <= h[i][j] && h[i + s - 1][j + s - 1] <= U)
				return true;
		}
	}
	return false;
}

int main() {
	FastIO();
	int q, i, j, lo, hi, s;
	while (cin >> n >> m, n || m) {
		for (i = 0; i < n; ++i) {
			for (j = 0; j < m; ++j)
				cin >> h[i][j];
		}
		cin >> q;
		while (q--) {
			cin >> L >> U;
			lo = 0, hi = min(m, n);
			while (lo < hi) {
				s = (lo + hi + 1)/2;
				if (side_possible(s)) {
					lo = s;
				} else {
					hi = s - 1;
				}
			}
			cout << lo << '\n';
		}
		cout << "-\n";
	}
	return 0;
}
