/**
	question: https://onlinejudge.org/external/105/10567.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cctype>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

vector<int> p[52];

int ph(char c) { // simple hashmap
	if (islower(c))
		return 26 + c - 'a';
	return c - 'A';
}

int main() {
	FastIO();
	string s, ss;
	int q, b, e, i;
	bool present;
	cin >> s >> q;
	for (i = 0; i < int(s.length()); ++i)
		p[ph(s[i])].push_back(i);
	while (q--) {
		cin >> ss;
		b = e = -1;
		present = true;
		for (char c: ss) {
			i = ph(c);
			auto pos = lower_bound(p[i].begin(), p[i].end(), e + 1);
			if (pos == p[i].end()) {
				present = false;
				break;
			}
			e = *pos;
			if (b == -1) b = e;
		}
		if  (present) cout << "Matched " << b << ' ' << e << '\n';
		else          cout << "Not matched\n";
	}
	return 0;
}
