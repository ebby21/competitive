/**
	question: https://onlinejudge.org/external/121/12192.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int H[500][500], N, M, L, U;

int main() {
	FastIO();
	int q, i, j, k, b;
	while (cin >> N >> M, N || M) {
		for (i = 0; i < N; ++i) {
			for (j = 0; j < M; ++j)
				cin >> H[i][j];
		}
		cin >> q;
		while (q--) {
			cin >> L >> U;
			b = 0;
			for (i = 0; i < N; ++i) {
				k = lower_bound(H[i], H[i] + M, L) - H[i] + b;
				j = i + b;
				while (j < N && k < M && H[j][k] <= U) {
					++b;
					++j;
					++k;
				}
			}
			cout << b << '\n';
		}
		cout << "-\n";
	}
	return 0;
}
