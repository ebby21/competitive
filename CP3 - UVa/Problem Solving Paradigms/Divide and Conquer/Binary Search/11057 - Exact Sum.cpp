/**
	question: https://onlinejudge.org/external/110/11057.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, m, i, x, y;
	vector<int> p;
	while (cin >> n) {
		p.resize(n);
		for (int &p_i: p) cin >> p_i;
		cin >> m;
		sort(p.begin(), p.end());
		x = 0, y = int(1e7);
		for (i = 0; i < n; ++i) {
			if (binary_search(p.begin() + i + 1, p.end(), m - p[i])
			|| binary_search(p.begin(), p.begin() + i + 1, m - p[i])) {
				if (abs(m - 2*p[i]) < y - x) {
					x = min(p[i], m - p[i]);
					y = max(p[i], m - p[i]);
				}
			}
		}
		cout << "Peter should buy books whose prices are " << x << " and " << y << ".\n\n";
	}
	return 0;
}
