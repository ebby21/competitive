/**
	question: https://onlinejudge.org/external/114/11413.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, m, i, lo, hi, x, p, k;
	vector<int> c;
	while (cin >> n >> m) {
		c.resize(n);
		for (int &c_i: c) cin >> c_i;
		/*	Reducing Search Space like this also
			tells us that all x can fit at least one container */
		lo = *max_element(c.begin(), c.end());
		hi = accumulate(c.begin(), c.end(), 0);
		while (lo < hi) {
			x = (lo + hi)/2;
			p = 0, k = 1;
			for (i = 0; i < n; ++i) {
				if (p + c[i] <= x) {
					p += c[i];
				} else {
					++k;
					p = c[i];
				}
				if (k > m) break;
			}
			if  (k <= m) hi = x;
			else         lo = x + 1;
		}
		cout << lo << '\n';
	}
	return 0;
}
