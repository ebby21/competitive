/**
	question: https://onlinejudge.org/external/103/10341.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <cmath>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const double Eps = 1e-8;

int p, q, r, s, t, u;

double f(double x) {
	return p*exp(-x) + q*sin(x) + r*cos(x) + s*tan(x) + t*x*x + u;
}

double f1(double x) {
	double sec_x = 1/cos(x);
	return -p*exp(-x) + q*cos(x) - r*sin(x) + s*sec_x*sec_x + 2*t*x;
}

int main() {
	FastIO();
	cout << fixed << setprecision(4);
	double x0, x;
	while (cin >> p >> q >> r >> s >> t >> u) {
		if (f(0.0)*f(1.0) > 0.0) {
			cout << "No solution\n";
			continue;
		}
		if (f(0) == 0) {
			x0 = 0.0;
		} else {
			x = 0.5;
			do {
				x0 = x;
				x = x0 - f(x0)/f1(x0);
			} while (abs(x - x0) >= Eps);
		}
		cout << x0 << '\n';
	}
	return 0;
}
