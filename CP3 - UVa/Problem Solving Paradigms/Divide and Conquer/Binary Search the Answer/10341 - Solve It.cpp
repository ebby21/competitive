/**
	question: https://onlinejudge.org/external/103/10341.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <cmath>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const double Eps = 1e-8;

int p, q, r, s, t, u;

double f(double x) {
	return p*exp(-x) + q*sin(x) + r*cos(x) + s*tan(x) + t*x*x + u;
}

int main() {
	FastIO();
	double lo, hi, x, y;
	cout << fixed << setprecision(4);
	while (cin >> p >> q >> r >> s >> t >> u) {
		lo = 0.0, hi = 1.0;
		if (f(lo)*f(hi) > 0.0) {
			cout << "No solution\n";
			continue;
		}
		while (abs(hi - lo) > Eps) {
			x = (lo + hi)/2;
			y = f(x);
			if      (y == 0.0) break;
			else if (y  > 0.0) lo = x;
			else               hi = x;
		}
		cout << lo << '\n';
	}
	return 0;
}
