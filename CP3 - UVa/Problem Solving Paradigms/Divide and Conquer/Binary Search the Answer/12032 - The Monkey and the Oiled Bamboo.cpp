/**
	question: https://onlinejudge.org/external/120/12032.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int _i = 1, t, n, lo, hi, k, i, d;
	bool enough;
	vector<int> r;
	cin >> t;
	while (t--) {
		cout << "Case " << _i++ << ": ";
		cin >> n;
		r.resize(n);
		for (int &r_i: r) cin >> r_i;
		lo = r[0], hi = r[n - 1];
		while (lo < hi) {
			k = (lo + hi)/2;
			enough = true;
			for (i = 0; i < n; ++i) {
				d = r[i];
				if (i > 0) d -= r[i - 1];
				if (k < d) {
					enough = false;
					break;
				}
				if (k == d) --k;
			}
			k = (lo + hi)/2;
			if  (enough) hi = k;
			else         lo = k + 1;
		}
		cout << lo << '\n';
	}
	return 0;
}
