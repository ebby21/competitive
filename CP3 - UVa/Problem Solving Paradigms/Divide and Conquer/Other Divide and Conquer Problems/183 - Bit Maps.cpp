/**
	question: http://uva.onlinejudge.org/external/1/183.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <string>
#include <limits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

int R, C;

string compress(const string &b, int r1=0, int r2=R-1, int c1=0, int c2=C-1) {
	if (r1 == r2 && c1 == c2) return string(1, b[r1*C + c1]);
	int r = (r1 + r2)/2, c = (c1 + c2)/2;
	string tl, tr, bl, br;
	tl = tr = bl = br = "";
	tl = compress(b, r1, r, c1, c);
	bool same = (tl.length() == 1);
	if (c < c2) {
		tr = compress(b, r1, r, c + 1, c2);
		if (tr != tl) same = false;
	}
	if (r < r2) {
		bl = compress(b, r + 1, r2, c1, c);
		if (bl != tl) same = false;
	}
	if (r < r2 && c < c2) {
		br = compress(b, r + 1, r2, c + 1, c2);
		if (br != tl) same = false;
	}
	if (same) return tl;
	return "D" + tl + tr + bl + br;
}

void decompress(string &o, int r1=0, int r2=R-1, int c1=0, int c2=C-1) {
	char d = cin.get();
	if (d != 'D') {
		for (int i = r1, j; i <= r2; ++i) {
			for (j = c1; j <= c2; ++j)
				o[i*C + j] = d;
		}
		return;
	}
	int r = (r1 + r2)/2, c = (c1 + c2)/2;
	decompress(o, r1, r, c1, c);
	if (c < c2) decompress(o, r1, r, c + 1, c2);
	if (r < r2) decompress(o, r + 1, r2, c1, c);
	if (r < r2 && c < c2) decompress(o, r + 1, r2, c + 1, c2);
}

int main() {
	FastIO();
	string i, o;
	unsigned int j;
	char m;
	while (cin >> m && m != '#') {
		cin >> R >> C;
		lose_newline(cin);
		if (m == 'B') {
			i = "";
			while (int(i.length()) < R*C) {
				getline(cin, o);
				i += o;
			}
			o = compress(i);
		} else {
			o.resize(R*C);
			decompress(o);
		}
		cout << (m == 'B'? 'D': 'B') << setw(4) << R << setw(4) << C;
		for (j = 0; j < o.size(); ++j) {
			if (j % 50 == 0) cout << '\n';
			cout << o[j];
		}
		cout << '\n';
	}
	return 0;
}
