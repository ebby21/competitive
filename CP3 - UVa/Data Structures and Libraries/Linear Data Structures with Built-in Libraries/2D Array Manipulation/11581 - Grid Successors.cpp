/**
	question: https://onlinejudge.org/external/115/11581.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>
#include <unordered_map>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef unsigned int ui;
typedef ui grid[3][3];

/* Generate bit state to represent grid in the hashmap */
ui get_state(grid &g) {
	ui s = 0;
	for (int i = 0, j; i < 3; ++i) {
		for (j = 0; j < 3; ++j)
			s |= (g[i][j] << (3*i + j));
	}
	return s;
}

void transform(grid &g) {
	grid t;
	for (int i = 0, j; i < 3; ++i) {
		for (j = 0; j < 3; ++j) {
			t[i][j] = 0;
			if (i - 1 >= 0) t[i][j] ^= g[i - 1][j];
			if (j - 1 >= 0) t[i][j] ^= g[i][j - 1];
			if (i + 1  < 3) t[i][j] ^= g[i + 1][j];
			if (j + 1  < 3) t[i][j] ^= g[i][j + 1];
		}
	}
	for (int i = 0, j; i < 3; ++i) {
		for (j = 0; j < 3; ++j)
			g[i][j] = t[i][j] % 2;
	}
}

int main() {
	FastIO();
	int t, i, j;
	ui s;
	grid g;
	char c;
	unordered_map<ui, int> state;
	cin >> t;
	while (t--) {
		state.clear();
		for (i = 0; i < 3; ++i) {
			for (j = 0; j < 3; ++j) {
				cin >> c;
				g[i][j] = c - '0';
			}
		}
		i = 0;
		do {
			s = get_state(g);
			if (state.find(s) != state.end()) {
				j = state[s] - 1;
				break;
			}
			state[s] = i++;
			transform(g);
		} while (true);
		cout << j << '\n';
	}
	return 0;
}
