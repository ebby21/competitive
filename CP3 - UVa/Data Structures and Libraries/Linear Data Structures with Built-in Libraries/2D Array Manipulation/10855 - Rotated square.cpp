/**
	question: https://onlinejudge.org/external/108/10855.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

void rotate90(vector<string> &g) {
	vector<string> orig(g);
	int n = g.size();
	for (int i = 0, j; i < n; ++i) {
		for (j = 0; j < n; ++j) {
			g[j][n - i - 1] = orig[i][j];
		}
	}
}

int count_grids(const vector<string> &b, const vector<string> &s) {
	int diff, n, count;
	bool present;
	n = s.size();
	diff = b.size() - n;
	count = 0;
	for (int i = 0; i <= diff; ++i) {
		for (int j = 0; j <= diff; ++j) {
			present = true;
			for (int r = 0; r < n; ++r) {
				for (int c = 0; c < n; ++c) {
					if (b[r + i][c + j] != s[r][c]) {
						present = false;
						break;
					}
				}
				if (!present) break;
			}
			if (present) ++count;
		}
	}
	return count;
}

int main() {
	FastIO();
	int N, n, c;
	vector<string> big, small;
	while (cin >> N >> n, N != 0 || n != 0) {
		big.resize(N);
		small.resize(n);
		for (string &s: big)   cin >> s;
		for (string &s: small) cin >> s;
		c = count_grids(big, small);
		cout << c;
		for (n = 0; n < 3; ++n) {
			rotate90(small);
			c = count_grids(big, small);
			cout << ' ' << c;
		}
		cout << '\n';
	}
	return 0;
}
