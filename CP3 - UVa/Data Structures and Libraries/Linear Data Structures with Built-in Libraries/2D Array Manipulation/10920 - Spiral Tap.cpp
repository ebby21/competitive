/**
	question: https://onlinejudge.org/external/109/10920.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>
#include <cassert>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int sz;
	long long p, n, Min, L, C;
	while (cin >> sz >> p, sz != 0 || p != 0) {
		n = sqrt(p);
		if (n*n  < p) ++n;
		if (n%2 == 0) ++n;
		Min = n*n - 4*n + 5;
		L = C = 0;
		if (Min <= p && p <= Min + n - 2) {
			L = n;
			C = n + Min - p - 1;
		} else if (Min + n - 2 < p && p <= Min + 2*n - 3) {
			L = Min + 2*n - 2 - p;
			C = 1;
		} else if (Min + 2*n - 3 < p && p <= Min + 3*n - 4) {
			L = 1;
			C = p - Min - 2*n + 4;
		} else {
			L = p - Min - 3*n + 5;
			C = n;
		}
		Min = (sz - 1)/2 - (n - 1)/2;
		L += Min;
		C += Min;
		cout << "Line = " << L << ", " << "column = " << C << ".\n";
	}
	return 0;
}
