/**
	question: https://onlinejudge.org/external/110/11034.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <queue>
#include <string>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

void process_queue(queue<int> &q, int l) {
	int len = 0, x;
	while (!q.empty()) {
		x = q.front();
		if ((len + x) > 100*l) break;
		q.pop();
		len += x;
	}
}

int main() {
	FastIO();
	int c, l, m, i, t, count;
	queue<int> lq, rq;
	string dir;
	cin >> c;
	while (c--) {
		cin >> l >> m;
		lq = queue<int>();
		rq = queue<int>();
		for (i = 0; i < m; ++i) {
			cin >> t >> dir;
			if  (dir == "left") lq.push(t);
			else                rq.push(t);
		}
		count = 0;
		while (!lq.empty() || !rq.empty()) {
			if (!lq.empty()) {
				process_queue(lq, l);
				++count;
			} else if (!rq.empty()) {
				++count;
			}
			if (!rq.empty()) {
				process_queue(rq, l);
				++count;
			} else if (!lq.empty()) {
				++count;
			}
		}
		cout << count << '\n';
	}
	return 0;
}
