/**
	question: https://onlinejudge.org/external/101/10172.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <queue>
#include <stack>
#include <array>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int St, n, i, Q, x, mins, total;
	unsigned int q, s;
	array<queue<int>, 100> aq;
	stack<int> car;
	cin >> St;
	while (St--) {
		cin >> n >> s >> q;
		total = 0;
		for (i = 0; i < n; ++i) {
			cin >> Q;
			total += Q;
			aq[i] = queue<int>();
			while (Q--) {
				cin >> x;
				aq[i].push(x - 1);
			}
		}
		car = stack<int>();
		i = Q = 0;
		mins = (total != 0? -2: 0);
		while (Q < total) {
			mins += 2;
			while (!car.empty()) {
				x = car.top();
				if (x != i) {
					if (aq[i].size() == q) break;
					aq[i].push(x);
				} else {
					++Q;
				}
				car.pop();
				++mins;
			}
			while (!aq[i].empty() && car.size() < s) {
				x = aq[i].front();
				aq[i].pop();
				car.push(x);
				++mins;
			}
			i = (i + 1)% n;
		}
		cout << mins << '\n';
	}
	return 0;
}
