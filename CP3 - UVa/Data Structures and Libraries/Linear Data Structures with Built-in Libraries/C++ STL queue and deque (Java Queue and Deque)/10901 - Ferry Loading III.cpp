/**
	question: https://onlinejudge.org/external/109/10901.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <queue>
#include <string>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef struct {
	int id, t;
} car;

void process_queue(queue<car> &q, int T, int n, int t, vector<int> &a) {
	int count = 0;
	car x;
	while (count < n && !q.empty()) {
		x = q.front();
		if (x.t > T) break;
		a[x.id] = T + t;
		q.pop();
		++count;
	}
}

int main() {
	FastIO();
	int c, n, t, m, i, T;
	queue<car> lq, rq;
	string dir;
	vector<int> ans;
	car x;
	bool left, move;
	cin >> c;
	while (c--) {
		cin >> n >> t >> m;
		lq = queue<car>();
		rq = queue<car>();
		for (i = 0; i < m; ++i) {
			x.id = i;
			cin >> x.t >> dir;
			if  (dir == "left") lq.push(x);
			else                rq.push(x);
		}
		ans.resize(m);
		T = 0;
		left = true;
		while (!lq.empty() || !rq.empty()) {
			move = false;

			if (!lq.empty()) {
				x = lq.front();
				if (x.t <= T) {
					move = true;
					if (!left) T += t;
					process_queue(lq, T, n, t, ans);
					left = false;
					T += t;
				}
			}
			if (!rq.empty()) {
				x = rq.front();
				if (x.t <= T) {
					move = true;
					if (left) T += t;
					process_queue(rq, T, n, t, ans);
					left = true;
					T += t;
				}
			}

			if (!move) ++T;
		}
		for (i = 0; i < m; ++i)
			cout << ans[i] << '\n';
		if (c != 0) cout << '\n';
	}
	return 0;
}
