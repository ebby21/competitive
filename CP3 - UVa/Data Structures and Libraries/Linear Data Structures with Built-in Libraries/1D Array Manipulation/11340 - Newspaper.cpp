/**
	question: https://onlinejudge.org/external/113/11340.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <limits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

int main() {
	FastIO();
	int n, k, m, cents;
	int value[256];
	unsigned char c; /* Tricky (has to be unsigned) */
	string line;
	cin >> n;
	while (n--) {
		fill(value, value + 256, 0);
		cin >> k;
		while (k--) {
			cin >> c;
			cin >> value[int(c)];
		}
		cin >> m;
		lose_newline(cin);
		cents = 0;
		while (m--) {
			getline(cin, line);
			for (unsigned char ch: line) /* ykw */
				cents += value[int(ch)];
		}
		cout << cents/100 << '.' << setfill('0') << setw(2) << cents%100 << "$\n";
	}
	return 0;
}
