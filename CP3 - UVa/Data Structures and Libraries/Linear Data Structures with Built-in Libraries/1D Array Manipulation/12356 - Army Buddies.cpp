/**
	question: https://onlinejudge.org/external/123/12356.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef struct {
	int L, R;
} sol;

int main() {
	FastIO();
	const int none = -1;
	int s, b, i, l, r;
	sol S[int(1e5)];
	while (cin >> s >> b, s != 0 || b != 0) {
		for (i = 0; i < s; ++i) {
			S[i].L = i - 1;
			S[i].R = i + 1;
		}
		S[0].L = S[s - 1].R = none;
		while (b--) {
			cin >> l >> r;
			--l, --r;
			if (S[l].L != none) {
				cout << S[l].L + 1;
				S[S[l].L].R = S[r].R;
			} else {
				cout << '*';
			}
			cout << ' ';
			if (S[r].R != none) {
				cout << S[r].R + 1;
				S[S[r].R].L = S[l].L;
			} else {
				cout << '*';
			}
			cout << '\n';
		}
		cout << "-\n";
	}
	return 0;
}
