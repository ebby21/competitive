/**
	question: https://onlinejudge.org/external/1/156.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
	int n, a, b, i;
	bool p[3000];
	while (cin >> n) {
		for (i = 1; i < n; ++i) p[i] = false;
		if (n > 0) cin >> a;
		for (i = 1; i < n; ++i) {
			cin >> b;
			p[abs(b - a)] = true;
			a = b;
		}
		p[0] = true;
		for (i = 1; i < n; ++i) {
			if (!p[i]) {
				p[0] = false;
				break;
			}
		}
		if  (p[0]) cout << "Jolly\n";
		else       cout << "Not jolly\n";
	}
	return 0;
}
