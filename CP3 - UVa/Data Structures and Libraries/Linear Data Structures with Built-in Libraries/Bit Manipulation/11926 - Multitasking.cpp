/**
	question: https://onlinejudge.org/external/119/11926.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <bitset>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	const int Size = int(1e6) + 1;
	int n, m, st, en, re, i;
	bool collide;
	bitset<Size> start, end;
	while (cin >> n >> m, n != 0 || m != 0) {
		start.reset();
		end.reset();
		collide = false;
		while (n--) {
			cin >> st >> en;
			if (collide) continue;
			if (start[st] || end[en]) {
				collide = true;
				continue;
			}
			start[st] = true;
			end[en] = true;
			for (i = st + 1; i < en; ++i) {
				if (!start[i] && !end[i]) {
					start[i] = end[i] = true;
				} else {
					collide = true;
					break;
				}
			}
		}
		while (m--) {
			cin >> st >> en >> re;
			if (collide) continue;
			en -= st;
			for (n = st; n < Size; n += re) {
				if (start[n]) {
					collide = true;
					continue;
				} else {
					start[n] = true;
				}
				if (n + en < Size) {
					if (end[n + en]) {
						collide = true;
						continue;
					} else {
						end[n + en] = true;
					}
				}
				for (i = n + 1; i < min(Size, n + en); ++i) {
					if (!start[i] && !end[i]) {
						start[i] = end[i] = true;
					} else {
						collide = true;
						break;
					}
				}
			}
		}
		if (!collide) cout << "NO ";
		cout << "CONFLICT\n";
	}
	return 0;
}
