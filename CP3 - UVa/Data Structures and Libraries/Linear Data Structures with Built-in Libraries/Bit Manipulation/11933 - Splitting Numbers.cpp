/**
	question: https://onlinejudge.org/external/119/11933.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	unsigned int n, a, b, t, k;
	while (cin >> n, n != 0) {
		t = 1;
		k = 0;
		a = b = 0;
		while (n > 0) {
			if (n & 1) {           // lsb of n is 1
				if (t & 1)         // t is odd
					a |= (1 << k);
				else
					b |= (1 << k);
				++t;
			}
			n >>= 1;
			++k;
		}
		cout << a << ' ' << b << '\n';
	}
	return 0;
}
