/**
	question: https://onlinejudge.org/external/102/10264.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int mwt, wt;
	unsigned int i, j, n;
	vector<int> w, p;
	while (cin >> n) {
		w.resize(1 << n);
		p.resize(1 << n);
		for (int &x: w) cin >> x;
		for (i = 0; i < w.size(); ++i) {
			p[i] = 0;
			for (j = 0; j < n; ++j)	// neighbouring corners have exactly one bit flipped
				p[i] += w[i ^ (1 << j)];
		}
		mwt = 0;
		for (i = 0; i < w.size(); ++i) {
			wt = 0;
			for (j = 0; j < n; ++j)
				wt = max(wt, p[i] + p[i ^ (1 << j)]);
			mwt = max(mwt, wt);
		}
		cout << mwt << '\n';
	}
	return 0;
}
