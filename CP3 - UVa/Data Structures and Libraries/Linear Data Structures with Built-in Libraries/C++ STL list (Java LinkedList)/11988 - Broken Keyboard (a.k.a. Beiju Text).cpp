/**
	question: https://onlinejudge.org/external/119/11988.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <list>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	string text;
	list<char> beiju;
	list<char>::iterator it;
	while (cin >> text) {
		beiju.clear();
		it = beiju.end();
		for (char c: text) {
			if (c == '[') {
				it = beiju.begin();
			} else if (c == ']') {
				it = beiju.end();
			} else {
				beiju.insert(it, c);
			}
		}
		for (char c: beiju) cout << c;
		cout << '\n';
	}
	return 0;
}
