/**
	question: https://onlinejudge.org/external/5/514.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <stack>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, i, c;
	stack<int> st;
	vector<int> a;
	bool possible;
	while (cin >> n, n != 0) {
		a.resize(n);
		while (cin >> a[0], a[0] != 0) {
			for (i = 1; i < n; ++i)
				cin >> a[i];
			i = 0, c = 1;
			possible = true;
			while (c <= n) {      // process incoming trains
				if (c == a[i]) {
					++i;
					++c;
				} else if (!st.empty() && st.top() == a[i]) {
					st.pop();
					++i;
				} else {
					st.push(c);
					++c;
				}
			}
			while (!st.empty()) { // process trains in the wait track
				if (st.top() != a[i]) possible = false;
				++i;
				st.pop();
			}
			cout << (possible? "Yes": "No") << '\n';
		}
		cout << '\n';
	}
	return 0;
}
