/**
	question: https://onlinejudge.org/external/10/1062.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	string s;
	int top[26];
	int _i = 1, ans;
	while (cin >> s, s != "end") {
		fill(top, top + 26, 0);
		for (char c: s) {
			for (int x = c - 'A'; x < 26; ++x) {
				if (top[x] > 0) {
					--top[x];
					break;
				}
			}
			++top[c - 'A'];
		}
		ans = count_if(top, top + 26, [](const int &x) { return x > 0; });
		cout << "Case " << _i++ << ": " << ans << '\n';
	}
	return 0;
}
