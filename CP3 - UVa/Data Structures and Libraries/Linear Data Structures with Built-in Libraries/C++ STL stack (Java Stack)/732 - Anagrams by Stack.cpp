/**
	question: https://onlinejudge.org/external/7/732.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <vector>
#include <stack>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

/* Naive approach will work (weak testcases) */
void try_all(string &s, string &t, string c, stack<char> &r, int i, int j, vector<string> &a) {
	if (!r.empty() && j < t.size() && r.top() == t[j]) {
		char top = r.top();
		r.pop();
		try_all(s, t, c + "o", r, i, j + 1, a);
		r.push(top);
	}
	if (i < s.size()) {
		r.push(s[i]);
		try_all(s, t, c + "i", r, i + 1, j, a);
		r.pop();
	}
	if (r.empty() && j >= t.size()) {
		a.push_back(c);
	}
}

vector<string> get_answer(string &s, string &t) {
	vector<string> a;
	stack<char> r;
	try_all(s, t, "", r, 0, 0, a);
	sort(a.begin(), a.end());
	return a;
}

int main() {
	FastIO();
	unsigned int i;
	string s, t;
	vector<string> ans;
	while (cin >> s >> t) {
		ans = get_answer(s, t);
		cout << "[\n";
		for (string &x: ans) {
			cout << x[0];
			for (i = 1; i < x.size(); ++i)
				cout << ' ' << x[i];
			cout << '\n';
		}
		cout << "]\n";
	}
	return 0;
}
