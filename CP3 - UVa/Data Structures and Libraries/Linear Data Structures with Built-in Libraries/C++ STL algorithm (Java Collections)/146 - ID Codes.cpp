/**
	question: https://onlinejudge.org/external/1/146.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <string>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	string s;
	while (cin >> s, s != "#") {
		if (next_permutation(s.begin(), s.end()))
			cout << s << '\n';
		else
			cout << "No Successor\n";
	}
	return 0;
}
