/**
	question: https://onlinejudge.org/external/102/10258.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <limits>
#include <algorithm>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

class contestant {
 public:
	int id, problem[9], time[9];
	bool submitted;

	void reset(int C) {
		id = C;
		fill(problem, problem + 9,  0);
		fill(time,    time    + 9, -1);
		submitted = false;
	}

	int score() const {
		return 9 - count(time, time + 9, -1);
	}

	int penalty() const {
		int pen = 0;
		for (int i = 0; i < 9; ++i) {
			if (time[i] != -1)
				pen += time[i] + 20*problem[i];
		}
		return pen;
	}

	bool operator<(const contestant &other) const {
		int s1 = this->score(), s2 = other.score();
		if (s1 > s2) return true;
		if (s1 < s2) return false;
		return this->penalty() < other.penalty();
	}
};

int main() {
	FastIO();
	int T, c, p, t;
	char l;
	string q;
	contestant co[100];
	cin >> T;
	lose_newline(cin);
	lose_newline(cin);
	while (T--) {
		for (c = 0; c < 100; ++c) co[c].reset(c + 1);
		while (getline(cin, q), q != "") {
			sscanf(q.c_str(), "%d %d %d %c", &c, &p, &t, &l);
			--c, --p; // 0-based indexing
			co[c].submitted = true;
			if (l != 'C' && l != 'I') continue; // doesn't affect score
			if (co[c].time[p] == -1) {
				if  (l == 'C') co[c].time[p] = t;
				else           ++co[c].problem[p];
			}
		}
		stable_sort(co, co + 100);
		l = 0;
		for (c = 0; c < 100; ++c) {
			if (co[c].submitted) {
				cout << co[c].id << ' ' << co[c].score() << ' ' << co[c].penalty() << '\n';
				l = 1;
			}
		}
		if (T != 0 && l) cout << '\n';
	}
	return 0;
}
