/**
	question: https://onlinejudge.org/external/101/10107.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>
#include <algorithm>
// #include <list>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int x, n = 0, med;
	array<int, int(1e4)> a;
	while (cin >> x) {
		a[n++] = x;
		nth_element(a.begin(), a.begin() + (n - 1)/2, a.begin() + n);
		med = a[(n - 1)/2];
		if (n % 2 == 0) {
			nth_element(a.begin(), a.begin() + n/2, a.begin() + n);
			med = (med + a[n/2])/ 2;
		}
		cout << med << '\n';
	}
	return 0;
}

/** Using std::list
int main() {
	FastIO();
	int x, n = 0, med;
	list<int> l;
	list<int>::iterator it;
	while (cin >> x) {
		it = l.begin();
		while (it != l.end() && *it <= x) ++it;
		l.insert(it, x);
		++n;
		it = l.begin();
		advance(it, (n - 1)/2);
		med = *it;
		if (n % 2 == 0) {
			++it;
			med = (med + *it)/2;
		}
		cout << med << '\n';
	}
	return 0;
} **/
