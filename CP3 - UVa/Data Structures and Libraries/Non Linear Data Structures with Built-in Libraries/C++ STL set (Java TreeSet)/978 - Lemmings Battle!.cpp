/**
	question: https://onlinejudge.org/external/9/978.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef multiset<int, greater<int>> rmset;

int main() {
	FastIO();
	int n, b, sb, sg, x, y, i;
	rmset blu, gre;
	rmset::iterator ib, ig;
	vector<int> vb, vg;
	cin >> n;
	while (n--) {
		cin >> b >> sg >> sb;
		blu.clear();
		gre.clear();
		while (sg--) { cin >> x; gre.insert(x); }
		while (sb--) { cin >> x; blu.insert(x); }
		while (!blu.empty() && !gre.empty()) {
			vb.clear();
			vg.clear();
			for (i = 0; i < b; ++i) {
				ib = blu.begin();
				ig = gre.begin();
				if (ib != blu.end() && ig != gre.end()) {
					x = *ib;
					y = *ig;
					blu.erase(ib);
					gre.erase(ig);
					if      (x > y) vb.push_back(x - y);
					else if (x < y) vg.push_back(y - x);
				} else {
					break;
				}
			}
			for (int &v: vb) blu.insert(v);
			for (int &v: vg) gre.insert(v);
		}
		if (!blu.empty()) {
			cout << "blue wins\n";
			for (const int &v: blu) cout << v << '\n';
		} else if (!gre.empty()) {
			cout << "green wins\n";
			for (const int &v: gre) cout << v << '\n';
		} else {
			cout << "green and blue died\n";
		}
		if (n != 0) cout << '\n';
	}
	return 0;
}
