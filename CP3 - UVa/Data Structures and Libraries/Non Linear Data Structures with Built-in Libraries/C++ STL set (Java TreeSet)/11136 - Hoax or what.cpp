/**
	question: https://onlinejudge.org/external/111/11136.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, k, x;
	unsigned long long price;
	multiset<int> bills;
	multiset<int>::iterator it;
	while (cin >> n, n != 0) {
		bills.clear();
		price = 0;
		while (n--) {
			cin >> k;
			while (k--) {
				cin >> x;
				bills.insert(x);
			}
			it = --bills.end(); // access last element in set
			price += *it;
			bills.erase(it);
			it = bills.begin();
			price -= *it;
			bills.erase(it);
		}
		cout << price << '\n';
	}
	return 0;
}
