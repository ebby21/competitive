/**
	question: https://onlinejudge.org/external/118/11849.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <unordered_set>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, m, x, count;
	unordered_set<int> cd;
	while (cin >> n >> m, n != 0 || m != 0) {
		cd.clear();
		while (n--) {
			cin >> x;
			cd.insert(x);
		}
		count = 0;
		while (m--) {
			cin >> x;
			if (cd.find(x) != cd.end())
				++count;
		}
		cout << count << '\n';
	}
	return 0;
}
