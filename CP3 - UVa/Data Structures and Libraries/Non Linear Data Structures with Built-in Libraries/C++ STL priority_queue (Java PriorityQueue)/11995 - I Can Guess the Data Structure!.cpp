/**
	question: https://onlinejudge.org/external/119/11995.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <stack>
#include <queue>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef priority_queue<int> MaxPQ;

template <typename T>
void check(T &C, int see, int x, bool &check) {
	C.pop();
	if (see != x) check = false;
}

int main() {
	FastIO();
	int n, t, x;
	bool st, qu, pq;
	MaxPQ P;
	queue<int> Q;
	stack<int> S;
	while (cin >> n) {
		st = qu = pq = true;
		P = MaxPQ();
		Q = queue<int>();
		S = stack<int>();
		while (n--) {
			cin >> t >> x;
			if (t == 1) {
				if (pq) P.push(x);
				if (qu) Q.push(x);
				if (st) S.push(x);
			} else {
				if (pq) {
					if  (!P.empty()) check(P, P.top(), x, pq);
					else             pq = false;
				}
				if (qu) {
					if  (!Q.empty()) check(Q, Q.front(), x, qu);
					else             qu = false;
				}
				if (st) {
					if  (!S.empty()) check(S, S.top(), x, st);
					else             st = false;
				}
			}
		}
		if      (!st && !qu && !pq) cout << "impossible";
		else if (!st && !qu)        cout << "priority queue";
		else if (!qu && !pq)        cout << "stack";
		else if (!st && !pq)        cout << "queue";
		else                        cout << "not sure";
		cout << '\n';
	}
	return 0;
}
