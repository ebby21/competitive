/**
	question: https://onlinejudge.org/external/12/1203.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <string>
#include <queue>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, pair<int, int>> iii;
typedef priority_queue<iii, vector<iii>, greater<iii>> MinPQ;

int main() {
	FastIO();
	int k, q, p;
	string s;
	MinPQ pq;
	iii x;
	while (cin >> s, s != "#") {
		cin >> q >> p;
		x = {p, {q, p}};
		pq.push(x);
	}
	cin >> k;
	while (k--) {
		x = pq.top();
		pq.pop();
		cout << x.second.first << '\n';
		x.first += x.second.second;
		pq.push(x);
	}
	return 0;
}
