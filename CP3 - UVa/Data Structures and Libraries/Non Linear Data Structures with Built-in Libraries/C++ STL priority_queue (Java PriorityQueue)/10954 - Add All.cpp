/**
	question: https://onlinejudge.org/external/109/10954.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <queue>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef priority_queue<int, vector<int>, greater<int>> MinPQ;

int main() {
	FastIO();
	int n, x, c;
	MinPQ pq;
	while (cin >> n, n != 0) {
		while (n--) {
			cin >> x;
			pq.push(x);
		}
		c = 0;
		while (pq.size() > 1) {
			x = pq.top();  pq.pop();
			x += pq.top(); pq.pop();
			c += x;
			pq.push(x);
		}
		cout << c << '\n';
		pq.pop();
	}
	return 0;
}
