/**
	question: https://onlinejudge.org/external/112/11286.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <algorithm>
#include <array>
#include <map>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int n, i, m, p;
	map<array<int, 5>, int> count;
	array<int, 5> x;
	while (cin >> n, n != 0) {
		count.clear();
		while (n--) {
			for (i = 0; i < 5; ++i)
				cin >> x[i];
			sort(x.begin(), x.end());
			++count[x];
		}
		m = 0;
		p = 0;
		for (auto &c: count) {
			if (c.second > p)
				p = m = c.second;
			else if (c.second == p)
				m += c.second;
		}
		cout << m << '\n';
	}
	return 0;
}
