/**
	question: https://onlinejudge.org/external/115/11572.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <unordered_map>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int t, n, i, m, c, p;
	unordered_map<int, vector<int>> sf; // faster than map, in this case
	vector<int> a;
	cin >> t;
	while (t--) {
		cin >> n;
		sf.clear();
		a.resize(n);
		for (i = 0; i < n; ++i) {
			cin >> a[i];
			vector<int> &r = sf[a[i]];
			if (r.empty()) r.push_back(1);
			r.push_back(i);
		}
		m = 0;
		if (n > 0) m = p = 1;

		/* A dp like solution, using previous result */
		for (i = 1; i < n; ++i) {
			vector<int> &r = sf[a[i]];
			if (i == r[1]) {
				c = p + 1;
			} else {
				if (r[r[0]] < (i - p)) // If prior index was before previous result
					c = p + 1;
				else                   // Otherwise find length from prior index
					c = i - r[r[0]];
				++r[0];
			}
			m = max(m, c);
			p = c;
		}
		cout << m << '\n';
		for (auto &s: sf) s.second.clear();
	}
	return 0;
}
