/**
	question: https://onlinejudge.org/external/102/10226.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <limits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

int main() {
	FastIO();
	int t, total;
	string s;
	map<string, int> freq;
	cin >> t;
	lose_newline(cin);
	lose_newline(cin);
	while (t--) {
		freq.clear();
		while (getline(cin, s)) {
			if (s == "") break;
			++freq[s];
		}
		total = 0;
		for (auto &f: freq)
			total += f.second;
		for (auto &f: freq)
			cout << f.first << ' ' << fixed << setprecision(4) << 100*f.second/double(total) << '\n';
		if (t != 0) cout << '\n';
	}
	return 0;
}
