/**
	question: https://onlinejudge.org/external/112/11235.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

template <typename T>
class SegmentTree {
	int N;
	vector<T> R;
	int left(int i)  { return (i << 1) + 1; }
	int right(int i) { return (i << 1) + 2; }
	static const T Z = -1;
	T combine(const T &x, const T &y) { return max(x, y); }
	void build(const vector<T> &A, int p, int l, int r) {
		if (l == r) {
			R[p] = A[l];
		} else {
			int m = (l + r)/2;
			build(A,  left(p),     l, m);
			build(A, right(p), m + 1, r);
			R[p] = combine(R[left(p)], R[right(p)]);
		}
	}
	T result(int p, int l, int r, int i, int j) {
		if (i > j) return Z;
		if (l == i && r == j) return R[p];
		int m = (l + r)/2;
		return combine(result( left(p),     l, m,             i, min(j, m)),
					   result(right(p), m + 1, r, max(i, m + 1),        j));
	}
 public:
	void build(const vector<T> &A) {
		N = A.size();
		R.resize(exp2(ceil(log2(N)) + 1));
		build(A, 0, 0, N - 1);
	}
	T result(int l, int r) {
		return result(0, 0, N - 1, l, r);
	}
};

int main() {
	FastIO();
	int n, q, i, j, k, ans;
	vector<int> a, st /* st[i] -> start position of a[i] */,
	            co /* co[i] -> total count of a[i] in a */;
	SegmentTree<int> maxq;
	while (cin >> n, n != 0) {
		cin >> q;
		a.resize(n);
		st.resize(n);
		co.assign(n, 0);
		for (i = 0; i < n; ++i) {
			cin >> a[i];
			if (i > 0 && a[i - 1] == a[i]) {
				st[i] = st[i - 1];
			} else {
				if (i > 0) co[i - 1] = i - st[i - 1];
				st[i] = i;
			}
		}
		co[n - 1] = n - st[n - 1];
		for (i = n - 2; i >= 0; --i) {
			if (co[i] == 0)
				co[i] = co[i + 1];
		}
		maxq.build(co);
		while (q--) {
			cin >> i >> j;
			--i, --j;
			if (a[i] == a[j]) { // easy case
				ans = j - i + 1;
			} else {
			/* Either the max is the group belonging to a[i],
			   or that of a[j], or somewhere in between */
				k = st[i] + co[i];
				ans = max(k - i, j - st[j] + 1);
				if (k < st[j] - 1)
					ans = max(ans, maxq.result(k, st[j] - 1));
			}
			cout << ans << '\n';
		}
	}
	return 0;
}
