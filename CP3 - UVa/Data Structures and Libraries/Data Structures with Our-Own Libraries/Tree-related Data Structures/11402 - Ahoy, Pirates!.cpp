/**
	question: https://onlinejudge.org/external/114/11402.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef enum {Nil, Set, Reset, Flip} state; // to be propagated

state inverse(state x) { // for flipping
	switch (x) {
		case Flip : return Nil;
		case Nil  : return Flip;
		case Set  : return Reset;
		case Reset: return Set;
	}
}

class SegmentTree {
	int N;
	vector<int> R;
	vector<state> S;
	int left(int i)  { return (i << 1) + 1; }
	int right(int i) { return (i << 1) + 2; }

	void build(const string &A, int p, int l, int r) {
		if (l == r) {
			R[p] = A[l] - '0';
		} else {
			int m = (l + r)/2;
			build(A,  left(p),     l, m);
			build(A, right(p), m + 1, r);
			R[p] = R[left(p)] + R[right(p)];
		}
	}

	void push(int p) {
		if (S[p] != Nil) {
			if (S[p] == Flip) {
				S[left(p)]  = inverse(S[left(p)]);
				S[right(p)] = inverse(S[right(p)]);
			} else {
				S[left(p)] = S[right(p)] = S[p];
			}
			S[p] = Nil;
		}
	}

	int sum(int p, int l, int r, int i, int j) {
		if (i > j) return 0;
		if (l == i && r == j) {
			if (S[p] == Nil) {
				return R[p];
			} else if (S[p] == Set) {
				return (r - l + 1);
			} else if (S[p] == Reset) {
				return 0;
			} else {
				return (r - l + 1 - R[p]);
			}
		} else {
			push(p);
			int m = (l + r)/2;
			int ans = sum(left(p), l, m, i, min(j, m)) + sum(right(p), m + 1, r, max(i, m + 1), j);
			R[p] = sum(left(p), l, m, l, m) + sum(right(p), m + 1, r, m + 1, r);
			return ans;
		}
	}

	void assign(int p, int l, int r, int i, int j, const state &v) {
		if (i > j) return;
		if (l == i && r == j) {
			S[p] = v;
		} else {
			push(p);
			int m = (l + r)/2;
			assign(left(p), l, m, i, min(j, m), v);
			assign(right(p), m + 1, r, max(i, m + 1), j, v);
			R[p] = sum(left(p), l, m, l, m) + sum(right(p), m + 1, r, m + 1, r);
		}
	}

	void invert(int p, int l, int r, int i, int j) {
		if (i > j) return;
		if (l == i && r == j) {
			S[p] = inverse(S[p]);
		} else {
			push(p);
			int m = (l + r)/2;
			invert(left(p), l, m, i, min(j, m));
			invert(right(p), m + 1, r, max(i, m + 1), j);
			R[p] = sum(left(p), l, m, l, m) + sum(right(p), m + 1, r, m + 1, r);
		}
	}
 public:
	void build(const string &A) {
		N = A.size();
		int size = exp2(ceil(log2(N)) + 1);
		R.resize(size);
		S.assign(size, Nil);
		build(A, 0, 0, N - 1);
	}
	int sum(int l, int r) {
		return sum(0, 0, N - 1, l, r);
	}
	void assign(int l, int r, const state &v) {
		assign(0, 0, N - 1, l, r, v);
	}
	void invert(int l, int r) {
		invert(0, 0, N - 1, l, r);
	}
};

int main() {
	FastIO();
	int T, m, q, t, n;
	string p, a = "";
	SegmentTree sumq;
	char op;
	cin >> T;
	for (int _i = 1; _i <= T; ++_i) {
		cout << "Case " << _i << ":\n";
		cin >> m;
		while (m--) {
			cin >> t >> p;
			while (t--) a += p;
		}
		sumq.build(a);
		a = "";
		cin >> q;
		t = 1;
		while (q--) {
			cin >> op >> m >> n;
			if (op == 'F') {
				sumq.assign(m, n, Set);
			} else if (op == 'E') {
				sumq.assign(m, n, Reset);
			} else if (op == 'I') {
				sumq.invert(m, n);
			} else {
				cout << "Q" << t++ << ": ";
				cout << sumq.sum(m, n) << '\n';
			}
		}
	}
	return 0;
}
