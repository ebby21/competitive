/**
	question: https://onlinejudge.org/external/125/12532.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <cmath>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

template <typename T>
class SegmentTree {
	int N;
	vector<T> R;
	int left(int i)  { return (i << 1) + 1; }
	int right(int i) { return (i << 1) + 2; }
	static const T Z = 1;
	T combine(const T &x, const T &y) { return x * y; }
	void build(const vector<T> &A, int p, int l, int r) {
		if (l == r) {
			R[p] = A[l];
		} else {
			int m = (l + r)/2;
			build(A,  left(p),     l, m);
			build(A, right(p), m + 1, r);
			R[p] = combine(R[left(p)], R[right(p)]);
		}
	}
	T result(int p, int l, int r, int i, int j) {
		if (i > j) return Z;
		if (l == i && r == j) return R[p];
		int m = (l + r)/2;
		return combine(result( left(p),     l, m,             i, min(j, m)),
					   result(right(p), m + 1, r, max(i, m + 1),        j));
	}
	void update(int p, int l, int r, int i, const T &v) {
		if (l == r) {
			R[p] = v;
		} else {
			int m = (l + r)/2;
			if  (i <= m) update( left(p),     l, m, i, v);
			else         update(right(p), m + 1, r, i, v);
			R[p] = combine(R[left(p)], R[right(p)]);
		}
	}
 public:
	void build(const vector<T> &A) {
		N = A.size();
		R.resize(exp2(ceil(log2(N)) + 1));
		build(A, 0, 0, N - 1);
	}
	T result(int l, int r) {
		return result(0, 0, N - 1, l, r);
	}
	void update(int i, const T &v) {
		update(0, 0, N - 1, i, v);
	}
};

void normalize(int &x) { // as we only care about sign
	if      (x < 0) x = -1;
	else if (x > 0) x = +1;
}

int main() {
	FastIO();
	int n, k, i, j;
	char op;
	vector<int> x;
	SegmentTree<int> productq;
	while (cin >> n >> k) {
		x.resize(n);
		for (int &y: x) {
			cin >> y;
			normalize(y);
		}
		productq.build(x);
		while (k--) {
			cin >> op >> i >> j;
			if (op == 'C') {
				normalize(j);
				productq.update(i - 1, j);
			} else {
				--i, --j;
				n = productq.result(i, j);
				if      (n < 0) cout << '-';
				else if (n > 0) cout << '+';
				else            cout << '0';
			}
		}
		cout << '\n';
	}
	return 0;
}
