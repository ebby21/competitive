/**
	question: https://onlinejudge.org/external/108/10895.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef pair<int, int> ii;
array<vector<ii>, int(1e4)> matrix, transpose;

int main() {
	FastIO();
	int n, m, i, j, r;
	while (cin >> m >> n) {
		for (i = 0; i < m; ++i) {
			cin >> r;
			matrix[i].resize(r);
			for (j = 0; j < r; ++j) cin >> matrix[i][j].first;
			for (j = 0; j < r; ++j) cin >> matrix[i][j].second;
		}
		for (i = 0; i < m; ++i) { // reversing graph in adjacency-list form
			r = matrix[i].size();
			for (j = 0; j < r; ++j) {
				transpose[matrix[i][j].first - 1].push_back({i + 1, matrix[i][j].second});
			}
		}
		cout << n << ' ' << m << '\n';
		for (j = 0; j < n; ++j) {
			r = transpose[j].size();
			cout << r;
			for (i = 0; i < r; ++i) cout << ' ' << transpose[j][i].first;
			cout << '\n';
			if (r > 0) cout << transpose[j][0].second;
			for (i = 1; i < r; ++i) cout << ' ' << transpose[j][i].second;
			cout << '\n';
		}
		for (vector<ii> &row: matrix)    row.clear();
		for (vector<ii> &row: transpose) row.clear();
	}
	return 0;
}
