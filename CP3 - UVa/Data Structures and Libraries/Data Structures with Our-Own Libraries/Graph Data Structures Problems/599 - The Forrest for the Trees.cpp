/**
	question: https://onlinejudge.org/external/5/599.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <bitset>
#include <string>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

int main() {
	FastIO();
	int t, trees, acorns;
	unsigned int i;
	char u, v;
	string ed;
	bitset<26> edge; // edge[i] -> is i part of any edge?
	cin >> t;
	while (t--) {
		edge.reset();
		trees = acorns = 0;
		while (cin >> ed, ed[0] != '*') {
			/* Since given graph is a forest,
			any edge reduces no. of components by 1.
			components = |V| - |E| */
			sscanf(ed.c_str(), "(%c,%c)", &u, &v);
			u -= 'A'; v -= 'A';
			edge[u] = edge[v] = true;
			--trees;
		}
		cin >> ed;
		for (i = 0; i < ed.size(); i += 2) {
			if (!edge[ed[i] - 'A'])
				++acorns;
			++trees;
		}
		trees -= acorns;
		cout << "There are " << trees << " tree(s) and " << acorns << " acorn(s).\n";
	}
	return 0;
}
