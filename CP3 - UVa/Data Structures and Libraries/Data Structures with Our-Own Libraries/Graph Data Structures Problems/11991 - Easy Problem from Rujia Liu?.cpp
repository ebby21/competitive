/**
	question: https://onlinejudge.org/external/119/11991.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <array>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

const int L = int(1e6);
array<vector<int>, L> o;

int main() {
	FastIO();
	int n, m, i, k, v;
	while (cin >> n >> m) {
		for (vector<int> &vec: o) vec.clear();
		for (i = 0; i < n; ++i) {
			cin >> v;
			o[v - 1].push_back(i);
		}
		for (i = 0; i < m; ++i) {
			cin >> k >> v;
			--v, --k;
			if (k >= int(o[v].size())) {
				cout << "0\n";
			} else {
				cout << o[v][k] + 1 << '\n';
			}
		}
	}
	return 0;
}
