/**
	question: https://onlinejudge.org/external/105/10507.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <array>
#include <bitset>
#include <vector>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

bitset<26> active;
array<bitset<26>, 26> edge;
char q[4];

int main() {
	FastIO();
	int n, m, i, x, y;
	bool change;
	vector<int> act;
	while (cin >> n) {
		cin >> m;
		active.reset();
		for (bitset<26> &ed: edge) ed.reset();
		cin >> q;
		for (i = 0; i < 3; ++i)
			active[q[i] - 'A'] = true;
		while (m--) {
			cin >> q;
			x = q[0] - 'A';
			y = q[1] - 'A';
			edge[x][y] = edge[y][x] = true;
		}
		x = 0;
		while (true) {
			change = false;
			act.clear();
			for (i = 0; i < 26; ++i) {
				if (edge[i].none() || active[i]) continue;
				m = 0;
				for (y = 0; y < 26; ++y) {
					if (edge[i][y] && active[y])
						++m;
				}
				if (m >= 3) {
					act.push_back(i); // don't immediately activate brain cell
					change = true;
				}
			}
			if (!change) break;
			for (int z: act) active[z] = true;
			++x;
		}
		change = true;
		for (i = 0; i < 26; ++i) {
			if (edge[i].any() && !active[i]) {
				change = false;
				break;
			}
		}
		if (n > int(active.count())) change = false;
		if  (change) cout << "WAKE UP IN, " << x << ", YEARS\n";
		else         cout << "THIS BRAIN NEVER WAKES UP\n";
	}
	return 0;
}
