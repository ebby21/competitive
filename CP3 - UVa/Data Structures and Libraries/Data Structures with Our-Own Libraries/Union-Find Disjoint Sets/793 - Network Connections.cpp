/**
	question: https://onlinejudge.org/external/7/793.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <numeric>
#include <limits>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

class DisjointSet {
	int N;
	vector<int> P, S;
 public:
	void reset(int no) {
		N = no;
		P.resize(N); S.resize(N);
		iota(P.begin(), P.end(), 0);
		fill(S.begin(), S.end(), 1);
	}

	DisjointSet(int no = 0) { if (no > 0) reset(no); }

	int findSet(int p) {
		int r = p, n;
		while (r != P[r]) r = P[r];
		while (p != r) {
			n    = P[p];
			P[p] = r;
			p    = n;
		}
		return r;
	}

	bool connected(int p, int q) { return findSet(p) == findSet(q); }

	void unionSet(int p, int q) {
		int r = findSet(p), s = findSet(q);
		if (r == s) return;
		if (S[r] < S[s]) {
			P[r]  = s;
			S[s] += S[r];
		} else {
			P[s]  = r;
			S[r] += S[s];
		}
		N--;
	}
};

int main() {
	FastIO();
	int t, n, u, v, suc, uns;
	char op;
	string p;
	DisjointSet c;
	cin >> t;
	while (t--) {
		cin >> n;
		c.reset(n);
		suc = uns = 0;
		lose_newline(cin);
		while (getline(cin, p)) {
			if (p == "") break;
			sscanf(p.c_str(), "%c %d %d", &op, &u, &v);
			--u, --v;
			if      (op == 'c')         c.unionSet(u, v);
			else if (c.connected(u, v)) ++suc;
			else                        ++uns;
		}
		cout << suc << "," << uns << '\n';
		if (t != 0) cout << '\n';
	}
	return 0;
}
