/**
	question: https://onlinejudge.org/external/115/11503.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <string>
#include <unordered_map>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

class DisjointSet {
	int N;
	vector<int> P, S;
 public:
	void reset(int no) {
		N = no;
		P.resize(N); S.resize(N);
		iota(P.begin(), P.end(), 0);
		fill(S.begin(), S.end(), 1);
	}

	DisjointSet(int no = 0) { if (no > 0) reset(no); }

	int findSet(int p) {
		int r = p, n;
		while (r != P[r]) r = P[r];
		while (p != r) {
			n    = P[p];
			P[p] = r;
			p    = n;
		}
		return r;
	}

	void unionSet(int p, int q) {
		int r = findSet(p), s = findSet(q);
		if (r == s) return;
		if (S[r] < S[s]) {
			P[r]  = s;
			S[s] += S[r];
		} else {
			P[s]  = r;
			S[r] += S[s];
		}
		N--;
	}

	int size(int i) { return S[findSet(i)]; }
};

int main() {
	FastIO();
	const int Max = int(1e5);
	int t, f, n;
	DisjointSet net;
	unordered_map<string, int> index;
	string g, h;
	cin >> t;
	while (t--) {
		cin >> f;
		net.reset(Max);
		index.clear();
		n = 0;
		while (f--) {
			cin >> g >> h;
			if (index.find(g) == index.end()) index[g] = n++;
			if (index.find(h) == index.end()) index[h] = n++;
			net.unionSet(index[g], index[h]);
			cout << net.size(index[g]) << '\n';
		}
	}
	return 0;
}
