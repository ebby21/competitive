/**
	question: https://onlinejudge.org/external/5/556.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

enum {NORTH, EAST, SOUTH, WEST};

template <typename T>
bool in_range(T x, T lo, T hi) { return lo <= x && x <= hi; }

void find_pos(int &y, int &x, int d) {
	switch (d) {
	 case NORTH: --y; break;
	 case EAST : ++x; break;
	 case SOUTH: ++y; break;
	 case WEST : --x; break;
	}
}

int main() {
	int b, w, count[5], i, j, r, c, d, r0, c0;
	vector<string> grid;
	vector< vector<int> > left;
	while (cin >> b >> w, b != 0 && w != 0) {
		grid.resize(b);
		left.resize(b);
		for (i = 0; i < b; ++i) {
			cin >> grid[i];
			left[i].resize(w);
			fill(left[i].begin(), left[i].end(), 0);
		}
		d = EAST;
		r = b - 1, c = 0;
		do {
			++left[r][c];
			d = (d + 1)%4; // anti-clockwise turn
			for (i = 0; i < 4; ++i) {
				r0 = r, c0 = c;
				find_pos(r0, c0, (d + 4 - i)%4); // 'i' clockwise turns
				if (in_range(r0, 0, b - 1) && in_range(c0, 0, w - 1) && grid[r0][c0] == '0') {
					r = r0;
					c = c0;
					d = (d + 4 - i)%4;
					break;
				}
			}
		} while (r != b - 1 || c != 0);
		fill(count, count + 5, 0);
		for (i = 0; i < b; ++i) {
			for (j = 0; j < w; ++j) {
				if (grid[i][j] == '0' && left[i][j] < 5)
					++count[left[i][j]];
			}
		}
		for (i = 0; i < 5; ++i)
			cout << setw(3) << count[i];
		cout << '\n';
		grid.clear();
		left.clear();
	}
	return 0;
}
