/**
	question: https://onlinejudge.org/external/120/12085.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int last(int x, int y) {
	int d = 1, y0 = y;
	while (x && y) {
		if (x == y) {
			if (y0 % d == 0) return d;
			return y0 % d;
		}
		d *= 10;
		x /= 10;
		y /= 10;
	}
	return 0;
}

int main() {
	int n;
	int C, P, B, _i = 1, l;
	while (cin >> n, n != 0) {
		cout << "Case " << _i++ << ":\n";

		if (n > 0) {
			--n;
			cin >> C;
			B = C;
		}
		while (n--) {
			P = C;
			cin >> C;
			if (C != P + 1) {
				if (B != P) {
					l = last(B, P);
					cout << '0' << B << '-';
					if  (l == 0) cout << P << '\n';
					else         cout << l << '\n';
				} else {
					cout << '0' << P << '\n';
				}
				B = C;
			}
		}
		if (C == P + 1) {
			l = last(B, C);
			cout << '0' << B << '-';
			if  (l == 0) cout << C << '\n';
			else         cout << l << '\n';
		} else {
			cout << '0' << C << '\n';
		}

		cout << '\n';
	}
	return 0;
}
