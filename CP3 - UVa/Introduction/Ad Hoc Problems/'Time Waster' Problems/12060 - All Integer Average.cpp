/**
	question: https://onlinejudge.org/external/120/12060.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <iomanip>

using namespace std;

int gcd(int a, int b) {
	int r;
	while (a != 0) {
		r = b%a;
		b = a;
		a = r;
	}
	return b;
}

int digits(int n) {
	int d = 0;
	while (n != 0) {
		++d;
		n /= 10;
	}
	return d;
}

int main() {
	int n, d, i, x, integral, _i = 1, g, di, dd;
	bool negative;
	while (cin >> d, d != 0) {
		n = 0;
		for (i = 0; i < d; ++i) {
			cin >> x;
			n += x;
		}
		if (n < 0) {
			n = -n;
			negative = true;
		} else {
			negative = false;
		}

		integral = n/d;
		n %= d;
		g = gcd(n, d);
		n /= g;
		d /= g;

		di = digits(integral) + (negative ? 2: 0);
		dd = digits(d);

		cout << "Case " << _i++ << ":\n";
		if (n != 0) cout << setw(di + dd) << n << '\n';
		if (negative) cout << "- ";
		if (integral != 0 || n == 0) cout << integral;
		if (n != 0) for (i = 0; i < dd; ++i) cout << '-';
		cout << '\n';
		if (n != 0) cout << setw(di + dd) << d << '\n';
	}
	return 0;
}
