/**
	question: https://onlinejudge.org/external/114/11459.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

using namespace std;

int main() {
	int t, a, b, c, m[101];
	vector<int> p;
	cin >> t;
	while (t--) {
		cin >> a >> b >> c;
		for (int i = 1; i <= 100; ++i) m[i] = i;
		while (b--) {
			int x, y;
			cin >> x >> y;
			m[x] = y;
		}
		p.resize(a);
		for (int &x : p) x = 1;
		int curr = 0, die;
		bool over = false;
		while (c--) {
			cin >> die;
			if (over) continue;
			p[curr] = m[min(100, p[curr] + die)];
			if (p[curr] == 100) over = true;
			curr = (curr + 1)%a;
		}
		for (int i = 0; i < a; ++i) {
			cout << "Position of player " << i + 1 << " is " << p[i] << ".\n" ;
		}
	}
	return 0;
}
