/**
	question: https://onlinejudge.org/external/101/10189.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
	int n, m, _i = 1;
	while (cin >> n >> m, n != 0) {
		vector<string> field(n);
		for (int i = 0; i < n; ++i) {
			cin >> field[i];
		}
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				if (field[i][j] == '*') continue;
				int count = 0;
				if (i > 0) {
					if (field[i - 1][j] == '*')                  ++count;
					if (j > 0     && field[i - 1][j - 1] == '*') ++count;
					if (j + 1 < m && field[i - 1][j + 1] == '*') ++count;
				}
				if (j > 0     && field[i][j - 1] == '*') ++count;
				if (j + 1 < m && field[i][j + 1] == '*') ++count;
				if (i + 1 < n) {
					if (field[i + 1][j] == '*')                  ++count;
					if (j > 0     && field[i + 1][j - 1] == '*') ++count;
					if (j + 1 < m && field[i + 1][j + 1] == '*') ++count;
				}
				field[i][j] = '0' + count;
			}
		}
		if (_i != 1) cout << '\n';
		cout << "Field #" << _i++ << ":\n";
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < m; ++j) {
				cout << field[i][j];
			}
			cout << '\n';
		}
	}
	return 0;
}
