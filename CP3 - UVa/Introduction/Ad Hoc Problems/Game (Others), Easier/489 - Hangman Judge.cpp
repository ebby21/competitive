/**
	question: https://onlinejudge.org/external/4/489.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <bitset>

using namespace std;

int main() {
	int r;
	string word, guess;
	bitset<26> letters, guessed;
	while (cin >> r, r != -1) {
		cin >> word >> guess;
		letters.reset();
		guessed.reset();
		for (char c : word) {
			letters[c - 'a'] = true;
		}
		int right = 0, wrong = 0;
		for (char g : guess) {
			if (letters[g - 'a'] && !guessed[g - 'a']) {
				guessed[g - 'a'] = true;
				++right;
			} else if (!letters[g - 'a']) {
				++wrong;
			}
			if (right == letters.count()) break;
			if (wrong == 7) break;
		}
		cout << "Round " << r << '\n';
		if      (right == letters.count()) cout << "You win.\n";
		else if (wrong >= 7)               cout << "You lose.\n";
		else                               cout << "You chickened out.\n";
	}
	return 0;
}
