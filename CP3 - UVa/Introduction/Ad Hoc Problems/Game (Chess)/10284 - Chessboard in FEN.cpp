/**
	question: https://onlinejudge.org/external/102/10284.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

bool not_occupied(char p) {
	return p == '_' || p == 'X';
}

void pawn_path(char board[8][8], int i, int j) {
	if      (board[i][j] == 'p' && i + 1 < 8) ++i;
	else if (board[i][j] == 'P' &&     i > 0) --i;
	else                                      return;
	if (j > 0     && not_occupied(board[i][j - 1])) board[i][j - 1] = 'X';
	if (j + 1 < 8 && not_occupied(board[i][j + 1])) board[i][j + 1] = 'X';
}

void rook_path(char board[8][8], int i, int j) {
	int x;

	x = i - 1; //	North
	while (x >= 0 && not_occupied(board[x][j])) board[x--][j] = 'X';

	x = j - 1; //	West
	while (x >= 0 && not_occupied(board[i][x])) board[i][x--] = 'X';

	x = i + 1; //	South
	while (x < 8 && not_occupied(board[x][j])) board[x++][j] = 'X';

	x = j + 1; //	East
	while (x < 8 && not_occupied(board[i][x])) board[i][x++] = 'X';
}

void bishop_path(char board[8][8], int i, int j) {
	int r, c;

	r = i - 1, c = j - 1; //	North-West
	while (r >= 0 && c >= 0 && not_occupied(board[r][c])) board[r--][c--] = 'X';

	r = i + 1, c = j - 1; //	South-West
	while (r < 8 && c >= 0 && not_occupied(board[r][c])) board[r++][c--] = 'X';

	r = i + 1, c = j + 1; //	South-East
	while (r < 8 && c < 8 && not_occupied(board[r][c])) board[r++][c++] = 'X';

	r = i - 1, c = j + 1; //	North-East
	while (r >= 0 && c < 8 && not_occupied(board[r][c])) board[r--][c++] = 'X';
}

void knight_path(char board[8][8], int i, int j) {
	int x;

	x = i - 2;
	if (x >= 0) {
	//	Northern
		if (j > 0     && not_occupied(board[x][j - 1])) board[x][j - 1] = 'X';
		if (j + 1 < 8 && not_occupied(board[x][j + 1])) board[x][j + 1] = 'X';
	}
	x = j - 2;
	if (x >= 0) {
	//	Western
		if (i > 0     && not_occupied(board[i - 1][x])) board[i - 1][x] = 'X';
		if (i + 1 < 8 && not_occupied(board[i + 1][x])) board[i + 1][x] = 'X';
	}
	x = i + 2;
	if (x < 8) {
	//	Southern
		if (j > 0     && not_occupied(board[x][j - 1])) board[x][j - 1] = 'X';
		if (j + 1 < 8 && not_occupied(board[x][j + 1])) board[x][j + 1] = 'X';
	}
	x = j + 2;
	if (x < 8) {
	//	Eastern
		if (i > 0     && not_occupied(board[i - 1][x])) board[i - 1][x] = 'X';
		if (i + 1 < 8 && not_occupied(board[i + 1][x])) board[i + 1][x] = 'X';
	}
}

void king_path(char board[8][8], int i, int j) {
	if (i > 0) {
		if (not_occupied(board[i - 1][j]))                  board[i - 1][j]     = 'X';
		if (j > 0     && not_occupied(board[i - 1][j - 1])) board[i - 1][j - 1] = 'X';
		if (j + 1 < 8 && not_occupied(board[i - 1][j + 1])) board[i - 1][j + 1] = 'X';
	}
	if (j > 0     && not_occupied(board[i][j - 1])) board[i][j - 1] = 'X';
	if (j + 1 < 8 && not_occupied(board[i][j + 1])) board[i][j + 1] = 'X';
	if (i + 1 < 8) {
		if (not_occupied(board[i + 1][j]))                  board[i + 1][j]     = 'X';
		if (j > 0     && not_occupied(board[i + 1][j - 1])) board[i + 1][j - 1] = 'X';
		if (j + 1 < 8 && not_occupied(board[i + 1][j + 1])) board[i + 1][j + 1] = 'X';
	}
}

int main() {
	string fen;
	char board[8][8];
	while (cin >> fen) {
		int i = 0, j = 0;
		for (char p : fen) {
			if (p >= '1' && p <= '8') {
				int k = p - '0';
				while (k--) board[i][j++] = '_';
			} else if (p == '/') {
				++i;
				j = 0;
			} else {
				board[i][j++] = p;
			}
		}
		for (i = 0; i < 8; ++i) {
			for (j = 0; j < 8; ++j) {
				switch (board[i][j]) {
				 case 'P': case 'p':
					pawn_path(board, i, j);
					break;
				 case 'R': case 'r':
					rook_path(board, i, j);
					break;
				 case 'N': case 'n':
					knight_path(board, i, j);
					break;
				 case 'B': case 'b':
					bishop_path(board, i, j);
					break;
				 case 'Q': case 'q':
					rook_path(board, i, j);
					bishop_path(board, i, j);
					break;
				 case 'K': case 'k':
					king_path(board, i, j);
					break;
				}
			}
		}
		int count = 0;
		for (i = 0; i < 8; ++i) {
			for (j = 0; j < 8; ++j) {
				if (board[i][j] == '_')
					++count;
			}
		}
		cout << count << '\n';
	}
	return 0;
}
