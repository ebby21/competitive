/**
	question: https://onlinejudge.org/external/2/278.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		char p;
		int m, n;
		cin >> p >> m >> n;
		int ans;
		if (p == 'r') {
			ans = min(m, n);
		} else if (p == 'k') {
			ans = ceil(m*n/2.0);
		} else if (p == 'Q') {
			ans  = min(m, n);
		} else {
			ans = ceil(m/2.0) * ceil(n/2.0);
		}
		cout << ans << '\n';
	}
	return 0;
}
