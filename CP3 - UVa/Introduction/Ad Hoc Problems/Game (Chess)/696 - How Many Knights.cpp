/**
	question: https://onlinejudge.org/external/6/696.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>

using namespace std;

int main() {
	int M, N;
	while (cin >> M >> N, M != 0 || N != 0) {
		int m, n;
		if  (N < M) m = N, n = M;
		else        m = M, n = N;

		int ans;
		if      (m == 1) ans = n;
		else if (m == 2) ans = n - n%4 + (n%4 == 1 ? 2 : (n%4 == 0 ? 0 : 4));
		else             ans = ceil(m*n/2.0);
		cout << ans << " knights may be placed on a " << M << " row " << N << " column board.\n";
	}
	return 0;
}
