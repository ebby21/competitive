/**
	question: https://onlinejudge.org/external/109/10945.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <cctype>

using namespace std;

bool is_palindrome(const string &s) {
	int i = 0, j = s.length() - 1;
	while (i < j) {
		if (s[i] != s[j])
			return false;
		++i, --j;
	}
	return true;
}

int main() {
	string s, p;
	while (true) {
		getline(cin, s);
		if (s == "DONE") break;
		p = string();
		for (char c: s) {
			if (isalpha(c)) {
				p += toupper(c);
			}
		}
		if  (is_palindrome(p)) cout << "You won't be eaten!\n";
		else                   cout << "Uh oh..\n";
	}
	return 0;
}
