/**
	question: https://onlinejudge.org/external/4/401.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>

using namespace std;

char mirror(char x) {
	switch (x) {
	 case 'A': return 'A';
	 case 'E': return '3';
	 case 'H': return 'H';
	 case 'I': return 'I';
	 case 'J': return 'L';
	 case 'L': return 'J';
	 case 'M': return 'M';
	 case 'O': return 'O';
	 case 'S': return '2';
	 case 'T': return 'T';
	 case 'U': return 'U';
	 case 'V': return 'V';
	 case 'W': return 'W';
	 case 'X': return 'X';
	 case 'Y': return 'Y';
	 case 'Z': return '5';
	 case '1': return '1';
	 case '2': return 'S';
	 case '3': return 'E';
	 case '5': return 'Z';
	 case '8': return '8';
	}
	return 0;
}

bool is_palindrome(const string &s) {
	int i = 0, j = s.length() - 1;
	while (i < j) {
		if (s[i] != s[j])
			return false;
		++i, --j;
	}
	return true;
}

bool is_mirror(const string &s) {
	int i = 0, j = s.length() - 1;
	while (i < s.length()) {
		if (s[i] != mirror(s[j]))
			return false;
		++i, --j;
	}
	return true;
}

int main() {
	string s;
	bool pal, mir;
	while (cin >> s) {
		pal = is_palindrome(s);
		mir = is_mirror(s);
		cout << s << " -- is ";
		if (pal && mir) {
			cout << "a mirrored palindrome";
		} else if (pal) {
			cout << "a regular palindrome";
		} else if (mir) {
			cout << "a mirrored string";
		} else {
			cout << "not a palindrome";
		}
		cout << ".\n\n";
	}
	return 0;
}
