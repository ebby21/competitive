/**
	question: https://onlinejudge.org/external/112/11221.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>
#include <limits>
#include <string>

#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

bool is_palindrome(const string &s) {
	int i = 0, j = s.length() - 1;
	while (i < j) {
		if (s[i] != s[j])
			return false;
		++i, --j;
	}
	return true;
}

int msqpal(string &s) {
	int n = s.size();
	int sq = sqrt(n);
	if (sq*sq != n) return -1;
	if (!is_palindrome(s)) return -1;
	for (int i = 0; i < sq; ++i) {
		for (int j = i + 1; j < sq; ++j) {
			swap(s[i*sq + j], s[j*sq + i]);
		}
	}
	if (!is_palindrome(s)) return -1;
	return sq;
}

int main() {
	int t, ans;
	cin >> t;
	string s, p;
	lose_newline(cin);
	for (int _i = 1; _i <= t; ++_i) {
		getline(cin, s);
		p = string();
		for (char c: s) {
			if (isalpha(c)) {
				p += c;
			}
		}
		ans = msqpal(p);
		cout << "Case #" << _i << ":\n";
		if  (ans == -1) cout << "No magic :(\n";
		else            cout << ans << '\n';
	}
	return 0;
}
