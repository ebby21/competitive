/**
	question: https://onlinejudge.org/external/4/454.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <limits>
#include <string>
#include <vector>
#include <algorithm>

#define fastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)
#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

bool anagram(string &a, string &b) {
	static int freq[256];
	fill(freq, freq + 256, 0);
	for (char c: a) {
		if (c != ' ') ++freq[c];
	}
	for (char c: b) {
		if (c != ' ') --freq[c];
	}
	return all_of(freq, freq + 256, [](const int &i) { return i == 0; });
}

int main() {
	fastIO();
	int n;
	string s;
	vector<string> w;
	cin >> n;
	lose_newline(cin);
	lose_newline(cin);
	while (n--) {
		w.clear();
		while (getline(cin, s) && s != "") {
			w.push_back(s);
		}
		sort(w.begin(), w.end());
		for (int i = 0; i < w.size(); ++i) {
			for (int j = i + 1; j < w.size(); ++j) {
				if (anagram(w[i], w[j])) {
					cout << w[i] << " = " << w[j] << '\n';
				}
			}
		}
		if (n != 0) cout << '\n';
	}
	return 0;
}
