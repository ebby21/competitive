/**
	question: https://onlinejudge.org/external/1/195.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <algorithm>
#include <cctype>

#define fastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

void write_all_permutations(int *freq, string pre="") {
	char c;
	bool nothing_left = true;
	for (int i = 0; i < 52; ++i) {
		if (freq[i] > 0) {
			if  (i % 2 == 0) c = 'A' + i/2;
			else             c = 'a' + i/2;
			--freq[i];
			write_all_permutations(freq, pre + c);
			++freq[i];
			nothing_left = false;
		}
	}
	if (nothing_left) cout << pre << '\n';
}

int main() {
	fastIO();
	int n, i;
	cin >> n;
	string s;
	int freq[52];
	while (n--) {
		cin >> s;
		fill(freq, freq + 52, 0);
		for (char c: s) {
			if (isupper(c)) {
				i = c - 'A';
				++freq[2*i];
			} else {
				i = c - 'a';
				++freq[2*i + 1];
			}
		}
		write_all_permutations(freq);
	}
	return 0;
}
