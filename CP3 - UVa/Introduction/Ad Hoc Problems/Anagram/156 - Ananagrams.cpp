/**
	question: https://onlinejudge.org/external/1/156.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <unordered_map>
#include <cctype>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
	string s, t;
	unordered_map<string, string> aag;
	while (cin >> s, s != "#") {
		t = "";
		for (char c: s) t += toupper(c);
		sort(t.begin(), t.end());
		auto i = aag.find(t);
		if (i == aag.end()) {
			aag[t] = s;
		} else {
			aag[t] = "";
		}
	}
	vector<string> aag_list;
	for (auto i: aag) {
		if (i.second != "")
			aag_list.push_back(i.second);
	}
	sort(aag_list.begin(), aag_list.end());
	for (auto w: aag_list) {
		cout << w << '\n';
	}
	return 0;
}
