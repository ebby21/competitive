/**
	question: https://onlinejudge.org/external/108/10812.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int n, s, d;
	double a, b;
	cin >> n;
	while (n--) {
		cin >> s >> d;
		a = (s + d)/2.0;
		b = (s - d)/2.0;
		s = (int)a;
		d = (int)b;
		if (s == a && s >= 0 && d == b && d >= 0) {
			cout << s << " " << d << "\n";
		} else {
			cout << "impossible\n";
		}
	}
	return 0;
}
