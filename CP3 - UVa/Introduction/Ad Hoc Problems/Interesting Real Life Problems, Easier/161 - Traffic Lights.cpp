/**
	question: https://onlinejudge.org/external/1/161.pdf

	language: C++ 11
	author  : ebby21
**/

#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

const int HR  = 3600;
const int MIN = 60;

int main() {
	int h, m, s;
	vector<int> d;
	while (true) {
		d.clear();
		while (scanf(" %d", &s), s != 0) {
			d.push_back(s);
		}
		if (d.empty()) break;
		s = 2 * *min_element(d.begin(), d.end());
		bool green;
		while (s <= 5*HR) {
			green = true;
			for (int x: d) {
				m = floor(s/(double)x);
				if ((m % 2 == 1) || (s + 5 >= (m + 1)*x)) {
				// If for instant 's', signal is not green
					green = false;
					break;
				}
			}
			if (green) break;
			++s;
		}
		if (s <= 5*HR) {
			h = s/HR;
			s %= HR;
			m = s/MIN;
			s %= MIN;
			printf("%02d:%02d:%02d\n", h, m, s);
		} else {
			puts("Signals fail to synchronise in 5 hours");
		}
	}
	return 0;
}
