/**
	question: https://onlinejudge.org/external/6/637.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cmath>

using namespace std;

bool in_range(int x, int lo, int hi) {
	return lo <= x && x <= hi;
}

void write(int f, int s, int n) {
	if  (in_range(f, 1, n)) cout << f;
	else                    cout << "Blank";
	cout << ", ";
	if  (in_range(s, 1, n)) cout << s;
	else                    cout << "Blank";
	cout << '\n';
}

int main() {
	int n, p;
	while (cin >> n, n != 0) {
		p = ceil(n/4.0);
		cout << "Printing order for " << n << " pages:\n";
		for (int i = 1, f1, f2, b1, b2; i <= p; ++i) {
			b1 = 2*i;
			f2 = 2*i - 1;
			b2 = 4*p - b1 + 1;
			f1 = 4*p - f2 + 1;
			if (in_range(f1, 1, n) || in_range(f2, 1, n)) {
				cout << "Sheet " << i << ", front: ";
				write(f1, f2, n);
			}
			if (in_range(b1, 1, n) || in_range(b2, 1, n)) {
				cout << "Sheet " << i << ", back : ";
				write(b1, b2, n);
			}
		}
	}
	return 0;
}
