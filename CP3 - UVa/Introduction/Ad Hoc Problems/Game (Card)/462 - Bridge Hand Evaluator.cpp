/**
	question: https://onlinejudge.org/external/4/462.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	char cards[13][3];
	while (cin >> cards[0]) {
		for (int i = 1; i < 13; ++i) {
			cin >> cards[i];
		}
		int points = 0, S_count, H_count, D_count, C_count;
		S_count = H_count = D_count = C_count = 0;
		for (int i = 0; i < 13; ++i) {
			if      (cards[i][0] == 'A') points += 4;
			else if (cards[i][0] == 'K') points += 3;
			else if (cards[i][0] == 'Q') points += 2;
			else if (cards[i][0] == 'J') points += 1;

			if      (cards[i][1] == 'S') ++S_count;
			else if (cards[i][1] == 'H') ++H_count;
			else if (cards[i][1] == 'D') ++D_count;
			else                         ++C_count;
		}
		bool S_stopped, H_stopped, D_stopped, C_stopped;
		S_stopped = H_stopped = D_stopped = C_stopped = false;
		for (int i = 0; i < 13; ++i) {
			int count;
			bool *stopped;
			if      (cards[i][1] == 'S') count = S_count, stopped = &S_stopped;
			else if (cards[i][1] == 'H') count = H_count, stopped = &H_stopped;
			else if (cards[i][1] == 'D') count = D_count, stopped = &D_stopped;
			else                         count = C_count, stopped = &C_stopped;

			if      (cards[i][0] == 'K' && count <= 1) --points;
			else if (cards[i][0] == 'Q' && count <= 2) --points;
			else if (cards[i][0] == 'J' && count <= 3) --points;

			if      (cards[i][0] == 'A')              *stopped = true;
			else if (cards[i][0] == 'K' && count > 1) *stopped = true;
			else if (cards[i][0] == 'Q' && count > 2) *stopped = true;
		}
		int extra_points = 0;
		if (S_count == 2) ++extra_points;
		if (H_count == 2) ++extra_points;
		if (D_count == 2) ++extra_points;
		if (C_count == 2) ++extra_points;

		if (S_count < 2) extra_points += 2;
		if (H_count < 2) extra_points += 2;
		if (D_count < 2) extra_points += 2;
		if (C_count < 2) extra_points += 2;

		if (points + extra_points < 14) {
			cout << "PASS\n";
		} else if (points >= 16 && S_stopped && H_stopped && D_stopped && C_stopped) {
			cout << "BID NO-TRUMP\n";
		} else {
			char most = 'S';
			int count = S_count;
			if (H_count > count) most = 'H', count = H_count;
			if (D_count > count) most = 'D', count = D_count;
			if (C_count > count) most = 'C', count = C_count;
			cout << "BID " << most << '\n';
		}
	}
	return 0;
}
