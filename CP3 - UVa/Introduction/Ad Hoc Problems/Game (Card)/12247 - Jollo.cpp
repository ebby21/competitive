/**
	question: https://onlinejudge.org/external/122/12247.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int next_after(int c, bool *status) {
	while (c < 52) {
		if (!status[c])
			return c + 1;
		++c;
	}
	return 53;
}

int main() {
	int A, B, C, X, Y;
	bool status[52] = { false };
	while (cin >> A >> B >> C >> X >> Y, A != 0) {
		status[A - 1] = status[B - 1] = status[C - 1] =
		status[X - 1] = status[Y - 1] = true;

		// Sort A, B, C
		if (C < A) swap(A, C);
		if (B < A) swap(A, B);
		if (C < B) swap(B, C);
		// Sort X, Y
		if (Y < X) swap(X, Y);

		int ans = 53;
		if      (X > C) ans = min(ans, next_after(0, status));
		else if (Y > C) ans = min(ans, next_after(C, status));

		if (X > B) ans = min(ans, next_after(B, status));

		if (ans == 53) ans = -1;

		cout << ans << '\n';

		status[A - 1] = status[B - 1] = status[C - 1] =
		status[X - 1] = status[Y - 1] = false;
	}
	return 0;
}
