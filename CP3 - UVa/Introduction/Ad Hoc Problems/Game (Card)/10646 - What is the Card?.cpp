/**
	question: https://onlinejudge.org/external/106/10646.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int t;
	cin >> t;
	for (int _I = 1; _I <= t; ++_I) {
		char cards[52][3];
		for (int i = 0; i < 52; ++i) {
			cin >> cards[i];
		}
		int top = 26, Y = 0;
		for (int i = 0; i < 3; ++i) {
			int X;
			if (cards[top][0] >= '2' && cards[top][0] <= '9') {
				X = cards[top][0] - '0';
			} else {
				X = 10;
			}
			Y += X;
			top -= 11 - X;
		}
		int ans;
		if (Y <= top + 1) {
			ans = Y - 1;
		} else {
			Y -= top + 1;
			ans = 26 + Y;
		}
		cout << "Case " << _I << ": " << cards[ans] << '\n';
	}
	return 0;
}
