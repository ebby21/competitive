/**
	question: https://onlinejudge.org/external/10/1061.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <cstring>

#define FastIO() ios::sync_with_stdio(false), cin.tie(nullptr), cout.tie(nullptr)

using namespace std;

typedef char group[4];

void find_bg(group x, bool &a, bool &b, bool &rh) {
	a = b = rh = false;
	for (int i = 0; x[i]; ++i) {
		if      (x[i] == 'A') a = true;
		else if (x[i] == 'B') b = true;
		else if (x[i] == '+') rh = true;
	}
}

bool is_possible(group p, group q, group c) {
	bool Ap, Aq, Ac, Bp, Bq, Bc, Rp, Rq, Rc;
	find_bg(p, Ap, Bp, Rp);
	find_bg(q, Aq, Bq, Rq);
	find_bg(c, Ac, Bc, Rc);
	if (Ac  && !Ap && !Aq) return false;
	if (Bc  && !Bp && !Bq) return false;
	if (Rc  && !Rp && !Rq) return false;
	if (Ac  && Bc  && !(Ap && Bq) && !(Aq && Bp)) return false;
	if (!Ac && !Bc && ((Ap && Bp) || (Aq && Bq))) return false;
	return true;
}

group all[8] = {"O-", "O+", "A-", "A+", "B-", "B+", "AB-", "AB+"};
void write_all(group p, group q, group c) {
	int i;
	bool written;
	char temp[] = "IMPOSSIBLE";
	if (p[0] == '?') {
		written = false;
		for (i = 0; i < 8; ++i) {
			if (is_possible(all[i], q, c)) {
				if (!written) {
					written = true;
					strcpy(temp, all[i]);
				} else {
					if (temp[0]) {
						cout << "{" << temp;
						temp[0] = 0;
					}
					cout << ", " << all[i];
				}
			}
		}
		if  (written && !temp[0]) cout << "} ";
		else                      cout << temp << " ";
	} else {
		cout << p << " ";
	}

	if (q[0] == '?') {
		written = false;
		for (i = 0; i < 8; ++i) {
			if (is_possible(p, all[i], c)) {
				if (!written) {
					written = true;
					strcpy(temp, all[i]);
				} else {
					if (temp[0]) {
						cout << "{" << temp;
						temp[0] = 0;
					}
					cout << ", " << all[i];
				}
			}
		}
		if  (written && !temp[0]) cout << "} ";
		else                      cout << temp << " ";
	} else {
		cout << q << " ";
	}

	if (c[0] == '?') {
		written = false;
		for (i = 0; i < 8; ++i) {
			if (is_possible(p, q, all[i])) {
				if (!written) {
					written = true;
					strcpy(temp, all[i]);
				} else {
					if (temp[0]) {
						cout << "{" << temp;
						temp[0] = 0;
					}
					cout << ", " << all[i];
				}
			}
		}
		if  (written && !temp[0]) cout << "}";
		else                      cout << temp;
	} else {
		cout << c << " ";
	}
}

int main() {
	FastIO();
	int _i = 1;
	group p, q, c;
	while (cin >> p >> q >> c, p[0] != 'E') {
		cout << "Case " << _i++ << ": ";
		write_all(p, q, c);
		cout << '\n';
	}
	return 0;
}
