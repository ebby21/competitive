/**
	question: https://onlinejudge.org/external/6/608.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;

int main() {
	int n;
	char possible[12], x, y;
	string a, b, v;
	cin >> n;
	while (n--) {
		fill(possible, possible + 12, 0);
		for (int i = 0; i < 3; ++i) {
			cin >> a >> b >> v;
			if (v == "even") {
				x = y = 'N';
			} else if (v == "up") {
				x = 'H';
				y = 'L';
			} else {
				x = 'L';
				y = 'H';
			}
			for (int j = 0; j < 12; ++j) {
				if (a.find('A' + j) != string::npos) {
					if (possible[j] != 'N')
						possible[j] = (possible[j] == y ? 'N' : x);
				} else if (b.find('A' + j) != string::npos) {
					if (possible[j] != 'N')
						possible[j] = (possible[j] == x ? 'N' : y);
				} else if (v != "even") {
					possible[j] = 'N';
				}
			}
		}
		for (int i = 0; i < 12; ++i) {
			if (possible[i] == 'H' || possible[i] == 'L') {
				cout << (char)('A' + i) << " is the counterfeit coin and it is ";
				if  (possible[i] == 'H') cout << "heavy.\n";
				else                     cout << "light.\n";
				break;
			}
		}
	}
	return 0;
}
