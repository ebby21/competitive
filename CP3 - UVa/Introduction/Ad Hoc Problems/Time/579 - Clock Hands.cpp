/**
	question: https://onlinejudge.org/external/5/579.pdf

	language: C++ 11
	author  : ebby21
**/

#include <cstdio>
#include <cmath>

using namespace std;

int main() {
	int h, m;
	while (scanf(" %d:%02d", &h, &m), h != 0) {
		double angle = abs(30*h - 6*m + m/2.0);
		if (angle > 180.0) angle = 360 - angle;
		printf("%0.03lf\n", angle);
	}
	return 0;
}
