/**
	question: https://onlinejudge.org/external/119/11947.pdf

	language: Java
	author  : ebby21
**/

import java.io.*;
import java.util.*;

class Main {
	static String readLn (int maxLg) {
	//	utility function to read from stdin
		byte lin[] = new byte [maxLg];
		int lg = 0, car = -1;

		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n')) break;
				lin [lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0)) return (null);  // eof
		return (new String (lin, 0, lg));
	}

	public static void main (String args[]) {
		Main solution = new Main();
		solution.begin();
	}

	void begin() {
		String input;
		GregorianCalendar cal;
		input = Main.readLn(255);
		int i = 1, n = Integer.parseInt(input), m, d, y;
		while (i <= n) {
			input = Main.readLn(255);
			m = Integer.parseInt(input.substring(0, 2)) - 1;
			d = Integer.parseInt(input.substring(2, 4));
			y = Integer.parseInt(input.substring(4));
			cal = new GregorianCalendar(y, m, d);
			cal.add(Calendar.DATE, 280);
			d = cal.get(Calendar.DATE);
			m = cal.get(Calendar.MONTH) + 1;
			y = cal.get(Calendar.YEAR);
			System.out.printf("%d %02d/%02d/%04d %s\n", i++, m, d, y, zodiac(d, m));
		}
	}

	String zodiac(int d, int m) {
		if ((m == 1  && d >= 21) || (m ==  2 && d <= 19)) return "aquarius";
		if ((m == 2  && d >= 20) || (m ==  3 && d <= 20)) return "pisces";
		if ((m == 3  && d >= 21) || (m ==  4 && d <= 20)) return "aries";
		if ((m == 4  && d >= 21) || (m ==  5 && d <= 21)) return "taurus";
		if ((m == 5  && d >= 22) || (m ==  6 && d <= 21)) return "gemini";
		if ((m == 6  && d >= 22) || (m ==  7 && d <= 22)) return "cancer";
		if ((m == 7  && d >= 23) || (m ==  8 && d <= 21)) return "leo";
		if ((m == 8  && d >= 22) || (m ==  9 && d <= 23)) return "virgo";
		if ((m == 9  && d >= 24) || (m == 10 && d <= 23)) return "libra";
		if ((m == 10 && d >= 24) || (m == 11 && d <= 22)) return "scorpio";
		if ((m == 11 && d >= 23) || (m == 12 && d <= 22)) return "sagittarius";
		return "capricorn";
	}
}
