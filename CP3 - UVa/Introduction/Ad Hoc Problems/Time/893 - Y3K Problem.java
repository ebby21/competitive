/**
	question: https://onlinejudge.org/external/8/893.pdf

	language: Java
	author  : ebby21
**/

import java.io.*;
import java.util.*;

class Main {
	static String readLn (int maxLg) {
	//	utility function to read from stdin
		byte lin[] = new byte [maxLg];
		int lg = 0, car = -1;

		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n')) break;
				lin [lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0)) return (null);  // eof
		return (new String (lin, 0, lg));
	}

	public static void main (String args[]) {
		Main solution = new Main();
		solution.begin();
	}

	void begin() {
		String input;
		StringTokenizer idata;
		GregorianCalendar cal;
		int d, m, y, n;

		while ((input = Main.readLn(255)) != null) {
			idata = new StringTokenizer(input);
			n = Integer.parseInt(idata.nextToken());
			d = Integer.parseInt(idata.nextToken());
			m = Integer.parseInt(idata.nextToken()) - 1;
			y = Integer.parseInt(idata.nextToken());
			if (d == 0) break;
			cal = new GregorianCalendar(y, m, d);
			cal.add(Calendar.DAY_OF_MONTH, n);
			d = cal.get(Calendar.DAY_OF_MONTH);
			m = cal.get(Calendar.MONTH) + 1;
			y = cal.get(Calendar.YEAR);
			System.out.println(d + " " + m + " " + y);
		}
	}
}
