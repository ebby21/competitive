/**
	question: https://onlinejudge.org/external/108/10813.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

bool bingo(int h[5][5], bool m[5][5], int i, int j) {
	bool bh = true, bv = true, mj = true, mn = true;
//	Check whether current row, column or the major or minor diagonal is set
	for (int k = 0; k < 5; ++k) {
		if (!m[k][j]) bv = false;
		if (!m[i][k]) bh = false;
		if (!m[k][k]) mj = false;
		if (!m[k][5 - k - 1]) mn = false;
	}
	return bh || bv || mj || mn;
}

int main() {
	int n, hand[5][5], x, ans;
	bool mark[5][5], found;
	cin >> n;
	hand[2][2] = 0;
	mark[2][2] = true;
	while (n--) {
		for (int i = 0; i < 5; ++i) {
			for (int j = 0; j < 5; ++j) {
				if (i == 2 && j == 2) continue;
				cin >> hand[i][j];
				mark[i][j] = false;
			}
		}
		ans = 0;
		for (int i, j, k = 0; k < 75; ++k) {
			cin >> x;
			if (ans != 0) continue;
			found = false;
			for (i = 0; i < 5; ++i) {
				for (j = 0; j < 5; ++j) {
					if (hand[i][j] == x) {
						mark[i][j] = true;
						found = true;
						break;
					}
				}
				if (found) break;
			}
			if (bingo(hand, mark, i, j)) {
				ans = k + 1;
			}
		}
		cout << "BINGO after " << ans << " numbers announced\n";
	}
	return 0;
}
