/**
	question: https://onlinejudge.org/external/5/584.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>

using namespace std;

int main() {
	string game;
	while (getline(cin, game), game != "Game Over") {
		int score = 0;
		int i = 0, frames = 0;
		while (i < game.size()) {
			if (game[i] >= '0' && game[i] <= '9') {
				if (game[i + 2] != '/') {
					++frames;
					score += game[i] - '0';
					i += 2;
					score += game[i] - '0';
				}
				i += 2;
			} else if (game[i] == '/') {
				++frames;
				score += 10;
				i += 2;
				if      (game[i] >= '0' && game[i] <= '9') score += game[i] - '0';
				else if (game[i] == '/')                   score += 10 - (game[i - 2] - '0');
				else                                       score += 10;
			} else {
				++frames;
				score += 10;
				i += 2;
				if      (game[i] >= '0' && game[i] <= '9') score += game[i] - '0';
				else if (game[i] == '/')                   score += 10 - (game[i - 2] - '0');
				else                                       score += 10;
				if      (game[i + 2] >= '0' && game[i + 2] <= '9') score += game[i + 2] - '0';
				else if (game[i + 2] == '/')                       score += 10 - (game[i] - '0');
				else                                               score += 10;
			}
			if (frames == 10) break;
		}
		cout << score << '\n';
	}
	return 0;
}
