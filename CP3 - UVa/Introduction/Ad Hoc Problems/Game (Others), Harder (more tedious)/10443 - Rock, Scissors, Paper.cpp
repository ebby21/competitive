/**
	question: https://onlinejudge.org/external/104/10443.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

char weak(char L) {
	switch (L) {
	 case 'R': return 'P';
	 case 'P': return 'S';
	 case 'S': return 'R';
	}
	return 'X';
}

int main() {
	int t, r, c, n;
	char grid[100][101], new_grid[100][101], w, a;
	cin >> t;
	for (int _i = 0; _i < t; ++_i) {
		cin >> r >> c >> n;
		for (int i = 0; i < r; ++i) {
			cin >> grid[i];
		}
		for (int k = 0; k < n; ++k) {
			for (int i = 0; i < r; ++i) {
				for (int j = 0; j < c; ++j) {
					a = grid[i][j];
					w = weak(a);
					if      (i > 0     && grid[i - 1][j] == w) a = w;
					else if (j > 0     && grid[i][j - 1] == w) a = w;
					else if (i + 1 < r && grid[i + 1][j] == w) a = w;
					else if (j + 1 < c && grid[i][j + 1] == w) a = w;
					new_grid[i][j] = a;
				}
			}
			swap(grid, new_grid);
		}
		if (_i != 0) cout << '\n';
		for (int i = 0; i < r; ++i) {
			cout << grid[i] << '\n';
		}
	}
	return 0;
}
