/**
	question: https://onlinejudge.org/external/114/11498.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>

using namespace std;

int main() {
	while (true) {
		int K;
		cin >> K;
		if (K == 0) break;
		int N, M;
		cin >> N >> M;
		while (K--) {
			int X, Y;
			cin >> X >> Y;
			string pos;
			if (X == N || Y == M) {
				pos = "divisa";
			} else {
				pos = "";
				if  (Y > M) pos += 'N';
				else        pos += 'S';
				if  (X > N) pos += 'E';
				else        pos += 'O';
			}
			cout << pos << '\n';
		}
	}
	return 0;
}
