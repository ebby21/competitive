/**
	question: https://onlinejudge.org/external/117/11727.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int T;
	cin >> T;
	for (int _I = 1; _I <= T; ++_I) {
		int x, y, z;
		cin >> x >> y >> z;
		if (x > z) swap(x, z);
		if (x > y) swap(x, y);
		if (y > z) swap(y, z);
		cout << "Case " << _I << ": " << y << '\n';
	}
	return 0;
}
