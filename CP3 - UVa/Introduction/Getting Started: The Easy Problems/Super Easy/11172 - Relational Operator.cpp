/**
	question: https://onlinejudge.org/external/111/11172.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int t;
	cin >> t;
	while (t--) {
		int a, b;
		cin >> a >> b;
		char rel;
		if      (a < b) rel = '<';
		else if (a > b) rel = '>';
		else            rel = '=';
		cout << rel << '\n';
	}
	return 0;
}
