/**
	question: https://onlinejudge.org/external/5/573.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int H, U, D, F;
	while (cin >> H >> U >> D >> F, H != 0) {
		int day = 0;
		double loss = F/100.0 * U, h = 0.0;
		while (true) {
			if (U - day*loss > 0.0)
				h += U - day*loss;
			++day;
			if (h > H) break;
			h -= D;
			if (h < 0.0) break;
		}
		if  (h < 0.0) cout << "failure on day " << day << '\n';
		else          cout << "success on day " << day << '\n';
	}
	return 0;
}
