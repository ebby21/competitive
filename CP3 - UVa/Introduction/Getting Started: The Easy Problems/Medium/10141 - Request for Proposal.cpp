/**
	question: https://onlinejudge.org/external/101/10141.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <limits>
#include <string>

#define lose_newline(cin) cin.ignore(numeric_limits<streamsize>::max(), '\n')

using namespace std;

int main() {
	int n, p, _I = 1;
	while (cin >> n >> p, n != 0 || p != 0) {
		string req;
		lose_newline(cin);
		for (int i = 0; i < n; ++i) {
		/*	All requirements are guaranteed to be from this list
			and there won't be any duplicates. Hence,
			there is no need to store all of these.	*/
			getline(cin, req);
		}
		double price = numeric_limits<double>::max();
		int compliance = 0;
		string name;
		for (int i = 0; i < p; ++i) {
			string proposal;
			double d;
			int r;
			getline(cin, proposal);
			cin >> d >> r;
			lose_newline(cin);
			for (int j = 0; j < r; ++j) {
				string sat;
				getline(cin, sat);
			}
			if ((r > compliance) || (r == compliance && d < price)) {
				name = proposal;
				compliance = r;
				price = d;
			}
		}
		if (_I != 1) cout << '\n';
		cout << "RFP #" << _I++ << '\n' << name << '\n';
	}
	return 0;
}
