/**
	question: https://onlinejudge.org/external/115/11507.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <string>

using namespace std;

int main() {
	int L;
	while (cin >> L, L != 0) {
		string action, dir = "+x";
		while (--L) {
			cin >> action;
			if (action == "No") continue;
			if (dir[1] == 'x') {
				dir[1] = action[1];
				if      (dir[0]    == '+') dir[0] = action[0];
				else if (action[0] == '+') dir[0] = '-';
				else                       dir[0] = '+';
			} else if (dir[1] == action[1]) {
				dir[1] = 'x';
				if  (dir[0] == action[0]) dir[0] = '-';
				else                      dir[0] = '+';
			}
		}
		cout << dir << '\n';
	}
	return 0;
}
