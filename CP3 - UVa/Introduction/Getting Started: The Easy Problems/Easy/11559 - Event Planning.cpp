/**
	question: https://onlinejudge.org/external/115/11559.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

using namespace std;

int main() {
	int N, B, H, W;
	while (cin >> N >> B >> H >> W) {
		int p, a, min_cost = -1;
		while (H--) {
			cin >> p;
			bool possible = false;
			for (int i = 0; i < W; ++i) {
				cin >> a;
				if (a >= N) possible = true;
			}
			if (possible && p*N <= B && (min_cost == -1 || p*N < min_cost)) {
				min_cost = p*N;
			}
		}
		if  (min_cost == -1) cout << "stay home\n";
		else                 cout << min_cost << '\n';
	}
	return 0;
}
