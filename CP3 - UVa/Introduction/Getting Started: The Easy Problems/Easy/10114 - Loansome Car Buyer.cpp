/**
	question: https://onlinejudge.org/external/101/10114.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>
#include <vector>

using namespace std;

int main() {
	while (true) {
		int dur, n;
		double down_payment, amount;
		cin >> dur >> down_payment >> amount >> n;
		if (dur < 0) break;
		vector< pair<int, double> > deprec(n);
		for (int i = 0; i < n; ++i) {
			cin >> deprec[i].first >> deprec[i].second;
		}
		double price = (1.0 - deprec[0].second) * (amount + down_payment),
		       loan = amount/dur;
		int time = 0, i = 0;
		while (less_equal<double>()(price, amount)) {
			++time;
			if (i + 1 < n && deprec[i + 1].first == time) {
				++i;
			}
			price *= 1.0 - deprec[i].second;
			amount -= loan;
		}
		cout << time << ' ' << (time == 1 ? "month" : "months") << '\n';
	}
	return 0;
}
