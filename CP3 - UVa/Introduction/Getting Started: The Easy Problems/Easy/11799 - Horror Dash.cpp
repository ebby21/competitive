/**
	question: https://onlinejudge.org/external/117/11799.pdf

	language: C++ 11
	author  : ebby21
**/

#include <iostream>

using namespace std;

int main() {
	int T;
	cin >> T;
	for (int _I = 1; _I <= T; ++_I) {
		int N, max_speed;
		cin >> N >> max_speed;
		while (--N) {
			int speed;
			cin >> speed;
			if (speed > max_speed)
				max_speed = speed;
		}
		cout << "Case " << _I << ": " << max_speed << '\n';
	}
	return 0;
}
